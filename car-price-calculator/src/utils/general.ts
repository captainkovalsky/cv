import { CreateStyled } from '@emotion/styled';

export const transientOptions: Parameters<CreateStyled>[1] = {
  shouldForwardProp: (propName: string) => !propName.startsWith('$'),
};

export const isEmpty = (obj: unknown | object): boolean => {
  if (typeof obj === 'object' && obj !== null) {
    return Object.keys(obj).length === 0;
  }
  return false;
};

export function freeze<T>(obj: object): T {
  return Object.freeze(obj) as T;
}

export default transientOptions;
