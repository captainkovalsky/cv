import { FullVehicleInfo, VehicleInfo } from '../models';

const RATE = 38.91;
const toUAH = (x: number): number => x * RATE;

function getTotalCost({
  distance,
  consumption,
  gasPrice,
  price,
}: VehicleInfo): number {
  const fuelCost = (distance * consumption * gasPrice) / 100;
  const fuelCostUSD = fuelCost / RATE;
  return price + fuelCostUSD; // USD
}

export function getPricePerKm(base: VehicleInfo): number {
  const totalCostUSD = getTotalCost(base);

  return toUAH(totalCostUSD / (base.distance ?? 1));
}

export function getFullPricePerKm({
  base,
  amortizationFee,
  sellable,
  sellPrice,
}: FullVehicleInfo): number {
  let totalCostUSD = getTotalCost(base);
  totalCostUSD += amortizationFee;

  if (sellable) {
    totalCostUSD -= sellPrice;
  }

  if (totalCostUSD <= 0) {
    return 0;
  }

  return toUAH(totalCostUSD / (base.distance ?? 1));
}
