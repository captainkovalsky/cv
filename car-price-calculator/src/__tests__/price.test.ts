import '@testing-library/jest-dom';

import { DEFAULT_INFO, DEFAULT_INFO_EXTENDED } from '../models';
import { getFullPricePerKm, getPricePerKm } from '../services/price';

describe('Price service', () => {
  it('should get basic price per km', () => {
    const price = getPricePerKm(DEFAULT_INFO);
    expect(price).toEqual(7.741);
  });

  it('should get full price per km', () => {
    const price = getFullPricePerKm(DEFAULT_INFO_EXTENDED);
    expect(price).toEqual(7.741);
  });

  it('should get full price per km with amortization', () => {
    const price = getFullPricePerKm({
      ...DEFAULT_INFO_EXTENDED,
      amortizationFee: 1000,
    });
    expect(price).toEqual(8.1301);
  });

  it('should get full price per km with amortization and sellable car', () => {
    const price = getFullPricePerKm({
      ...DEFAULT_INFO_EXTENDED,
      amortizationFee: 1000,
      sellPrice: 5000,
      sellable: true,
    });
    expect(price).toEqual(6.1846);
  });
});
