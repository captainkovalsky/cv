import {
  Box,
  Flex,
  Tab,
  TabIndicator,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
} from '@chakra-ui/react';

import BasicForm from './components/BasicForm';
import FullForm from './components/FullForm';
import ThemeToggleButton from './components/ThemeToggleButton';

const textFontSizes = [16, 18, 24, 30];

function App(): JSX.Element {
  return (
    <Box>
      <Flex
        as="header"
        direction="column"
        alignItems="center"
        justifyContent="center"
        h="100vh"
        fontSize="3xl"
      >
        <Text fontSize={textFontSizes}>Калькулятор ціни кілометра</Text>
        <Tabs
          borderWidth="1px"
          borderRadius="lg"
          isFitted
          colorScheme="green"
          size="lg"
          position="relative"
          isLazy
          defaultIndex={0}
        >
          <TabList mb="1em">
            <Tab>Основний</Tab>
            <Tab>Розширений</Tab>
          </TabList>
          <TabIndicator
            mt="-33px"
            height="2px"
            bg="green.500"
            borderRadius="1px"
          />
          <TabPanels>
            <TabPanel>
              <BasicForm />
            </TabPanel>
            <TabPanel>
              <FullForm />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Flex>
      <ThemeToggleButton pos="fixed" bottom="2" right="2" />
    </Box>
  );
}

export default App;
