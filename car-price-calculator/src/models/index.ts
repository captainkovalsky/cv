import { z } from 'zod';

import { freeze } from '../utils/general';

export const DEFAULT_INFO = {
  price: 10_000,
  distance: 100_000,
  gasPrice: 55,
  consumption: 7,
};

export const DEFAULT_INFO_EXTENDED = {
  base: { ...DEFAULT_INFO },
  amortizationFee: 1000,
  sellPrice: DEFAULT_INFO.price,
  sellable: false,
};

const VehicleZod = z.object({
  price: z.coerce
    .number()
    .gte(1000, { message: 'min is 1000 $' })
    .lte(300_000, { message: 'max is 300000 $' })
    .default(DEFAULT_INFO.price),
  distance: z.coerce
    .number()
    .gte(1, { message: 'min is 1km' })
    .lte(1_000_000, { message: 'max is 1 million km' })
    .default(DEFAULT_INFO.distance),
  gasPrice: z.coerce
    .number()
    .gte(20, 'min is 20 UAH')
    .lte(200, 'max is 200 UAH')
    .default(DEFAULT_INFO.gasPrice),
  consumption: z.coerce
    .number()
    .gte(1, 'consumption should be more than 1')
    .lte(100, 'are you using plane?')
    .default(DEFAULT_INFO.consumption),
});

const FullVehicleInfoZod = z.object({
  base: VehicleZod,
  amortizationFee: z.coerce
    .number()
    .default(DEFAULT_INFO_EXTENDED.amortizationFee),
  sellPrice: z.coerce.number().default(DEFAULT_INFO_EXTENDED.sellPrice),
  sellable: z.coerce.boolean().optional(),
});

export const FormikShape = z.object({
  full: FullVehicleInfoZod,
  base: VehicleZod,
});

export type VehicleInfo = z.infer<typeof VehicleZod>;
export type FullVehicleInfo = z.infer<typeof FullVehicleInfoZod>;
export type TForm = z.infer<typeof FormikShape>;

export const initialValues = freeze<TForm>({
  base: { ...DEFAULT_INFO },
  full: { ...DEFAULT_INFO_EXTENDED },
});
