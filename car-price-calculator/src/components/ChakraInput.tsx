import { FieldProps } from 'formik';
import { JSX } from 'react';

import {
  Input,
  InputGroup,
  InputLeftElement,
  InputLeftElementProps,
} from '@chakra-ui/react';

const addonsProps: Partial<InputLeftElementProps> = {
  pointerEvents: 'none',
  color: 'green.500',
  left: '10px',
};

interface ChakraNumberInputProps extends FieldProps<number> {
  addon: string;
  id: string;
}

type ChakraNumberInputFunction = (p: ChakraNumberInputProps) => JSX.Element;

export function ChakraNumberInput({
  field,
  form,
  ...props
}: ChakraNumberInputProps) {
  const { addon } = props;
  return (
    <InputGroup>
      {addon && <InputLeftElement {...addonsProps}>{addon}</InputLeftElement>}
      <Input
        type="number"
        name={field.name}
        onChange={form.handleChange}
        paddingLeft="55px"
        {...props}
      />
    </InputGroup>
  );
}

export const WithAddon = (
  Component: ChakraNumberInputFunction,
  addon: string
): ChakraNumberInputFunction =>
  function ChakraNumberInputWithAddon(props) {
    return <Component {...props} addon={addon} />;
  };

export const WithUsdInput = WithAddon(ChakraNumberInput, '$');
export const WithUahCurrencyInput = WithAddon(ChakraNumberInput, '₴');
export const WithDistanceInput = WithAddon(ChakraNumberInput, 'Km');
export const WithConsumptionSignInput = WithAddon(ChakraNumberInput, 'L');
