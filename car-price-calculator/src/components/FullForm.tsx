import { Field, FieldProps, Form, Formik, FormikState } from 'formik';
import { toFormikValidate, toFormikValidationSchema } from 'zod-formik-adapter';

import {
  Box,
  Checkbox,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Stat,
  StatLabel,
  StatNumber,
} from '@chakra-ui/react';

import {
  DEFAULT_INFO_EXTENDED,
  FormikShape,
  initialValues,
  TForm,
} from '../models';
import { getFullPricePerKm } from '../services/price';
import { isEmpty } from '../utils/general';

import {
  WithConsumptionSignInput,
  WithDistanceInput,
  WithUahCurrencyInput,
  WithUsdInput,
} from './ChakraInput';

export default function FullForm() {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={toFormikValidationSchema(FormikShape)}
      validateOnBlur
      validateOnChange
      validate={toFormikValidate(FormikShape)}
      onSubmit={() => undefined}
    >
      {({ errors, values }: FormikState<TForm>) => {
        const fullFormErrors = errors.full ?? {};
        return (
          <Form>
            <FormControl isInvalid={!!fullFormErrors?.base?.price}>
              <FormLabel>Ціна машини</FormLabel>
              <Field
                id="full.base.price"
                name="full.base.price"
                defaultValue={DEFAULT_INFO_EXTENDED.base.price}
                component={WithUsdInput}
              />
              <FormErrorMessage>{fullFormErrors?.base?.price}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!fullFormErrors?.base?.distance}>
              <FormLabel>Кілометраж</FormLabel>
              <Field
                id="full.base.distance"
                name="full.base.distance"
                defaultValue={DEFAULT_INFO_EXTENDED.base.distance}
                component={WithDistanceInput}
              />

              <FormErrorMessage>
                {fullFormErrors?.base?.distance}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!fullFormErrors?.base?.gasPrice}>
              <FormLabel>Ціна пального</FormLabel>
              <Field
                id="full.base.gasPrice"
                name="full.base.gasPrice"
                defaultValue={DEFAULT_INFO_EXTENDED.base.gasPrice}
                component={WithUahCurrencyInput}
              />
              <FormErrorMessage>
                {fullFormErrors?.base?.gasPrice}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!fullFormErrors?.base?.consumption}>
              <FormLabel>Витрата на 100км</FormLabel>
              <Field
                id="full.base.consumption"
                name="full.base.consumption"
                defaultValue={DEFAULT_INFO_EXTENDED.base.consumption}
                component={WithConsumptionSignInput}
              />
              <FormErrorMessage>
                {fullFormErrors?.base?.consumption}
              </FormErrorMessage>
            </FormControl>

            <FormControl isInvalid={!!fullFormErrors?.amortizationFee}>
              <FormLabel>Амортизаційні витрати</FormLabel>
              <Field
                id="full.amortizationFee"
                name="full.amortizationFee"
                defaultValue={DEFAULT_INFO_EXTENDED.amortizationFee}
                component={WithUsdInput}
              />
              <FormErrorMessage>
                {fullFormErrors?.amortizationFee}
              </FormErrorMessage>
            </FormControl>
            <FormControl>
              <Field
                id="full.sellable"
                name="full.sellable"
                type="boolean"
                defaultValue={DEFAULT_INFO_EXTENDED.sellable}
              >
                {({ field, form, ...props }: FieldProps<boolean>) => (
                  <Box paddingTop={5}>
                    <Checkbox
                      colorScheme="green"
                      onChange={form.handleChange}
                      name={field.name}
                      {...props}
                    >
                      <FormLabel m={0}> Я планую продати машину</FormLabel>
                    </Checkbox>
                  </Box>
                )}
              </Field>
            </FormControl>
            <FormControl
              isInvalid={!!fullFormErrors?.sellPrice}
              isDisabled={!values.full.sellable}
            >
              <FormLabel>Ціна продажу</FormLabel>
              <Field
                id="full.sellPrice"
                name="full.sellPrice"
                defaultValue={DEFAULT_INFO_EXTENDED.sellPrice}
                component={WithUsdInput}
              />
              <FormErrorMessage>{fullFormErrors?.sellPrice}</FormErrorMessage>
            </FormControl>

            <Stat marginTop="20px">
              <StatLabel>Ціна кілометра</StatLabel>
              <StatNumber>
                {isEmpty(fullFormErrors)
                  ? getFullPricePerKm(
                      values?.full ?? DEFAULT_INFO_EXTENDED
                    ).toFixed(2)
                  : 0}{' '}
                UAH
              </StatNumber>
            </Stat>
          </Form>
        );
      }}
    </Formik>
  );
}
