import { Field, Form, Formik, FormikState } from 'formik';
import { toFormikValidate, toFormikValidationSchema } from 'zod-formik-adapter';

import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Stat,
  StatLabel,
  StatNumber,
} from '@chakra-ui/react';

import { DEFAULT_INFO, FormikShape, initialValues, TForm } from '../models';
import { getPricePerKm } from '../services/price';
import { isEmpty } from '../utils/general';

import {
  WithConsumptionSignInput,
  WithDistanceInput,
  WithUahCurrencyInput,
  WithUsdInput,
} from './ChakraInput';

export default function BasicForm() {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={toFormikValidationSchema(FormikShape)}
      validateOnBlur
      validateOnChange
      validate={toFormikValidate(FormikShape)}
      onSubmit={() => undefined}
    >
      {({ errors, values }: FormikState<TForm>) => {
        const baseFormErrors = errors.base ?? {};
        return (
          <Form>
            <FormControl isInvalid={!!baseFormErrors?.price}>
              <FormLabel>Ціна машини</FormLabel>
              <Field
                id="base.price"
                name="base.price"
                defaultValue={DEFAULT_INFO.price}
                component={WithUsdInput}
              />
              <FormErrorMessage>{baseFormErrors?.price}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!baseFormErrors?.distance}>
              <FormLabel>Кілометраж</FormLabel>
              <Field
                id="base.distance"
                name="base.distance"
                defaultValue={DEFAULT_INFO.distance}
                component={WithDistanceInput}
              />
              <FormErrorMessage>{baseFormErrors?.distance}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!baseFormErrors?.gasPrice}>
              <FormLabel>Ціна пального</FormLabel>
              <Field
                id="base.gasPrice"
                name="base.gasPrice"
                defaultValue={DEFAULT_INFO.gasPrice}
                component={WithUahCurrencyInput}
              />
              <FormErrorMessage>{baseFormErrors?.gasPrice}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!baseFormErrors?.consumption}>
              <FormLabel>Витрата на 100км</FormLabel>
              <Field
                id="base.consumption"
                name="base.consumption"
                defaultValue={DEFAULT_INFO.consumption}
                component={WithConsumptionSignInput}
              />
              <FormErrorMessage>{baseFormErrors?.consumption}</FormErrorMessage>
            </FormControl>
            <Stat>
              <StatLabel>Ціна кілометра</StatLabel>
              <StatNumber>
                {isEmpty(baseFormErrors)
                  ? getPricePerKm(values.base).toFixed(2)
                  : 0}{' '}
                UAH
              </StatNumber>
            </Stat>
          </Form>
        );
      }}
    </Formik>
  );
}
