#!/bin/sh

username="$1"

if [ -n "$2" ]; then
    CR_PAT="$2"
else
    CR_PAT="$CR_PAT_ENV"
fi

export CR_PAT

if [ -z "$CR_PAT" ]; then
    echo "Failed: CR_PAT is not set. Provide via CR_PAT_ENV or pass like an arg."
    exit 1
fi

echo "$CR_PAT" | docker login ghcr.io -u "$username" --password-stdin
