#!/bin/sh

total_commits=$(git rev-list --count HEAD)
desired_commit=$((total_commits - 1))
git reset --soft $(git rev-parse HEAD~$desired_commit)
git commit -S -a -m "main"
git push --force-with-lease
# verify GPG
