#!/bin/sh

owner="$1"
repo="$2"
commit_hash="$3"
image_name="quay.io/$owner/$repo"

if [ -n "$commit_hash" ]; then
    docker commit "$commit_hash" "$image_name"
    docker push "$image_name"
else
    echo "Example: $0 <owner> <repository> <commit_hash>"
fi