use bevy::{
    prelude::*,
};
use colored::Colorize;

fn main() {
    println!("{}", "Start App".color("red"));
    App::new()
        .add_plugins((DefaultPlugins
            .set(WindowPlugin {
                primary_window: Some(Window {
                    canvas: Some("#bevy".to_string()),
                    ..default()
                }),
                ..default()
            })
            .set(AssetPlugin {
                watch_for_changes_override: Option::from(true),
                ..Default::default()
            }),))
        .add_systems(Startup, setup)
        .run();
}

const X_EXTENT: f32 = 800.;

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands.spawn(Camera2dBundle::default());

    let scale = 0.3f32;
    let fruit_textures = [
        asset_server.load("fruit-02.png"),
        asset_server.load("fruit-03.png"),
        asset_server.load("fruit-04.png"),
        asset_server.load("fruit-05.png"),
        asset_server.load("fruit-06.png"),
        asset_server.load("fruit-07.png"),
    ];

    const MAX_ITEMS_PER_ROW: usize = 4;
    const X_OFFSET: f32 = 300.0;
    const Y_OFFSET: f32 = 300.0;

    for (i, sprite) in fruit_textures.into_iter().enumerate() {
        let row = i / MAX_ITEMS_PER_ROW;
        let col = i % MAX_ITEMS_PER_ROW;

        let shift_x = col as f32 * X_OFFSET;
        let shift_y = row as f32 * Y_OFFSET;

        commands.spawn(SpriteBundle {
            transform: Transform {
                scale: Vec3::new(scale, scale, 0.0),
                translation: Vec3::new(-X_EXTENT / 2.0 + shift_x, shift_y, 0.0),
                ..Default::default()
            },
            texture: sprite,
            ..Default::default()
        });
    }
}
