use crate::entities::{DistinctParties, LocationAreaCode, LocationParts, Locations, RegDocuments};
use crate::mappers;
use crate::mappers::XmlParseError;
use crate::models::{DistinctPartyTable, LocationTable, RegDocDocumentTable};
use serde::Deserialize;
use std::collections::HashMap;

pub fn deserialize_locations<'a, 'b, R>(
    deserializer: &'a mut quick_xml::de::Deserializer<'b, R>,
) -> Result<HashMap<i64, LocationTable>, XmlParseError>
where
    R: quick_xml::de::XmlRead<'b>,
{
    let mut list: HashMap<i64, LocationTable> = HashMap::new();
    let mut parsed_locations: HashMap<LocationParts, String> = Default::default();

    let obj = Locations::deserialize(&mut *deserializer);
    match obj {
        Ok(data) => {
            let locations: Locations = data;
            let locations = locations.locations;

            for process_location in locations {
                let location_country = &process_location.location_country;

                let location_id: i64 = process_location.id;
                let location_area_code_id: i64 = process_location
                    .location_area_code
                    .unwrap_or(LocationAreaCode { area_code_id: -1 })
                    .area_code_id;
                let mut country_id = None;
                let mut country_relevance_id = None;
                let mut location_text = "".to_string();

                match location_country {
                    None => {}
                    Some(country) => {
                        country_id = Option::from(country.country_id);
                        country_relevance_id = Option::from(country.country_relevance_id);
                    }
                }

                let location_parts = &process_location.location_parts;
                match location_parts {
                    None => {
                        // println!(
                        //     "{} {} {}",
                        //     "[WARNING]".yellow(),
                        //     "no location parts for ",
                        //     location_id
                        // );
                    }
                    Some(parts) => {
                        parsed_locations = mappers::map_location_parts(parts);
                        let joined = parsed_locations
                            .values()
                            .cloned()
                            .collect::<Vec<String>>()
                            .join(", ");
                        location_text = joined;
                    }
                }

                list.insert(
                    location_id,
                    LocationTable {
                        id: location_id,
                        location_area_code_id,
                        country_id,
                        country_relevance_id,
                        location_text,
                        region: parsed_locations.get(&LocationParts::Region).cloned(),
                        address_1: parsed_locations.get(&LocationParts::Address1).cloned(),
                        address_2: parsed_locations.get(&LocationParts::Address2).cloned(),
                        address_3: parsed_locations.get(&LocationParts::Address3).cloned(),
                        city: parsed_locations.get(&LocationParts::City).cloned(),
                        state: parsed_locations.get(&LocationParts::State).cloned(),
                        postal_code: parsed_locations.get(&LocationParts::PostalCode).cloned(),
                    },
                );
            }
        }
        Err(err) => {
            return Err(XmlParseError::ParseLocations(format!("{:?}", err)));
        }
    }

    Ok(list)
}

pub fn deserialize_reg_documents<'a, 'b, R>(
    deserializer: &'a mut quick_xml::de::Deserializer<'b, R>,
) -> Result<HashMap<i64, RegDocDocumentTable>, XmlParseError>
where
    R: quick_xml::de::XmlRead<'b>,
{
    let mut list: HashMap<i64, RegDocDocumentTable> = HashMap::new();
    let obj = RegDocuments::deserialize(&mut *deserializer);
    match obj {
        Ok(root) => {
            for doc in root.documents {
                list.insert(
                    doc.id,
                    RegDocDocumentTable {
                        id: doc.id,
                        id_reg_doc_type_id: doc.id_reg_doc_type_id,
                        identity_id: doc.identity_id,
                        validity_id: doc.validity_id,
                    },
                );
            }
        }
        Err(err) => {
            eprintln!("err {:?}", err);
        }
    }
    Ok(list)
}

pub fn deserialize_distinct_parties<'a, 'b, R>(
    deserializer: &'a mut quick_xml::de::Deserializer<'b, R>,
) -> Result<HashMap<i64, DistinctPartyTable>, XmlParseError>
where
    R: quick_xml::de::XmlRead<'b>,
{
    let mut list: HashMap<i64, DistinctPartyTable> = HashMap::new();
    let obj = DistinctParties::deserialize(&mut *deserializer);
    match obj {
        Ok(root) => {
            for part in root.parties {
                list.insert(part.id, DistinctPartyTable { id: part.id });
            }
        }
        Err(err) => {
            eprintln!("err {:?}", err);
        }
    }
    Ok(list)
}

#[cfg(test)]
mod tests {

    use crate::models::{DistinctPartyTable, LocationTable, RegDocDocumentTable};
    use pretty_assertions::assert_eq;
    use quick_xml::de::Deserializer;

    #[test]
    fn deserialize_locations() {
        let xml = r#"
            <Root>
                <Location ID="25">
                    <LocationAreaCode AreaCodeID="11073" />
                    <LocationCountry CountryID="11073" CountryRelevanceID="1413" />
                    <LocationPart LocPartTypeID="1454">
                        <LocationPartValue Primary="true" LocPartValueTypeID="1" LocPartValueStatusID="1">
                            <Comment />
                            <Value>Havana</Value>
                        </LocationPartValue>
                    </LocationPart>
                    <FeatureVersionReference FeatureVersionID="200025" />
                </Location>
            </Root>
        "#;

        let mut des = Deserializer::from_str(xml);
        let result = crate::deserializers::deserialize_locations(&mut des);
        match result {
            Ok(ref list) => {
                assert_eq!(1, list.len());
                let (_id, actual) = list.iter().next().unwrap();

                let expected = &LocationTable {
                    id: 25,
                    location_area_code_id: 11073,
                    country_id: Some(11073),
                    country_relevance_id: Some(1413),
                    location_text: "Havana".to_string(),
                    region: None,
                    address_1: None,
                    address_2: None,
                    address_3: None,
                    city: Some("Havana".to_string()),
                    state: None,
                    postal_code: None,
                };

                assert_eq!(actual, expected);
            }

            _ => {}
        }
    }

    #[test]
    fn deserialize_reg_documents() {
        let xml = r#"
        <Root>
            <IDRegDocument ID="22133" IDRegDocTypeID="1626" IdentityID="1668" ValidityID="1">
                <Comment />
                <IDRegistrationNo>IMO 7406784</IDRegistrationNo>
                <IssuingAuthority />
                <DocumentedNameReference DocumentedNameID="1668" />
            </IDRegDocument>
        </Root>
        "#;

        let mut des = Deserializer::from_str(xml);
        let result = crate::deserializers::deserialize_reg_documents(&mut des);

        match result {
            Ok(ref list) => {
                assert_eq!(list.len(), 1);
                let (_id, actual) = list.iter().next().unwrap();
                assert_eq!(
                    actual,
                    &RegDocDocumentTable {
                        id: 22133,
                        id_reg_doc_type_id: 1626,
                        identity_id: 1668,
                        validity_id: 1,
                    }
                )
            }

            _ => {}
        }
    }

    #[test]
    fn deserialize_distinct_parties() {
        let xml = r#"
        <Root>
            <DistinctParty FixedRef="2676">
              <Comment />
              <Profile ID="2676" PartySubTypeID="4">
                <Identity ID="4422" FixedRef="2676" Primary="true" False="false">
                  <Alias FixedRef="2676" AliasTypeID="1403" Primary="true" LowQuality="false">
                    <DocumentedName ID="4422" FixedRef="2676" DocNameStatusID="1">
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="71646" ScriptID="215" ScriptStatusID="1" Acronym="false">AL ZAWAHIRI</NamePartValue>
                      </DocumentedNamePart>
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="71647" ScriptID="215" ScriptStatusID="1" Acronym="false">Dr. Ayman</NamePartValue>
                      </DocumentedNamePart>
                    </DocumentedName>
                  </Alias>
                  <Alias FixedRef="2676" AliasTypeID="1400" Primary="false" LowQuality="false">
                    <DocumentedName ID="13219" FixedRef="2676" DocNameStatusID="2">
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19445" ScriptID="215" ScriptStatusID="1" Acronym="false">AL-ZAWAHIRI</NamePartValue>
                      </DocumentedNamePart>
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19446" ScriptID="215" ScriptStatusID="1" Acronym="false">Ayman</NamePartValue>
                      </DocumentedNamePart>
                    </DocumentedName>
                  </Alias>
                  <Alias FixedRef="2676" AliasTypeID="1400" Primary="false" LowQuality="false">
                    <DocumentedName ID="13220" FixedRef="2676" DocNameStatusID="2">
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19447" ScriptID="215" ScriptStatusID="1" Acronym="false">SALIM</NamePartValue>
                      </DocumentedNamePart>
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19448" ScriptID="215" ScriptStatusID="1" Acronym="false">Ahmad Fuad</NamePartValue>
                      </DocumentedNamePart>
                    </DocumentedName>
                  </Alias>
                  <Alias FixedRef="2676" AliasTypeID="1400" Primary="false" LowQuality="false">
                    <DocumentedName ID="13221" FixedRef="2676" DocNameStatusID="2">
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19449" ScriptID="215" ScriptStatusID="1" Acronym="false">AL-ZAWAHIRI</NamePartValue>
                      </DocumentedNamePart>
                      <DocumentedNamePart>
                        <NamePartValue NamePartGroupID="19450" ScriptID="215" ScriptStatusID="1" Acronym="false">Aiman Muhammad Rabi</NamePartValue>
                      </DocumentedNamePart>
                    </DocumentedName>
                  </Alias>
                  <NamePartGroups>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="71646" NamePartTypeID="1520" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="71647" NamePartTypeID="1521" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19445" NamePartTypeID="1520" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19446" NamePartTypeID="1521" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19447" NamePartTypeID="1520" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19448" NamePartTypeID="1521" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19449" NamePartTypeID="1520" />
                    </MasterNamePartGroup>
                    <MasterNamePartGroup>
                      <NamePartGroup ID="19450" NamePartTypeID="1521" />
                    </MasterNamePartGroup>
                  </NamePartGroups>
                </Identity>
                <Feature ID="1003" FeatureTypeID="8">
                  <FeatureVersion ID="3553" ReliabilityID="1">
                    <Comment />
                    <DatePeriod CalendarTypeID="1" YearFixed="false" MonthFixed="false" DayFixed="false">
                      <Start Approximate="false" YearFixed="false" MonthFixed="false" DayFixed="false">
                        <From>
                          <Year>1951</Year>
                          <Month>6</Month>
                          <Day>19</Day>
                        </From>
                        <To>
                          <Year>1951</Year>
                          <Month>6</Month>
                          <Day>19</Day>
                        </To>
                      </Start>
                      <End Approximate="false" YearFixed="false" MonthFixed="false" DayFixed="false">
                        <From>
                          <Year>1951</Year>
                          <Month>6</Month>
                          <Day>19</Day>
                        </From>
                        <To>
                          <Year>1951</Year>
                          <Month>6</Month>
                          <Day>19</Day>
                        </To>
                      </End>
                    </DatePeriod>
                    <VersionDetail DetailTypeID="1430" />
                  </FeatureVersion>
                  <IdentityReference IdentityID="4422" IdentityFeatureLinkTypeID="1" />
                </Feature>
                <Feature ID="1004" FeatureTypeID="9">
                  <FeatureVersion ID="3554" ReliabilityID="1">
                    <Comment />
                    <VersionDetail DetailTypeID="1432">Giza, Egypt</VersionDetail>
                  </FeatureVersion>
                  <IdentityReference IdentityID="4422" IdentityFeatureLinkTypeID="1" />
                </Feature>
                <Feature ID="151834" FeatureTypeID="25">
                  <FeatureVersion ID="201834" ReliabilityID="1">
                    <Comment />
                    <VersionLocation LocationID="1834" />
                  </FeatureVersion>
                  <IdentityReference IdentityID="4422" IdentityFeatureLinkTypeID="1" />
                </Feature>
                <Feature ID="302676" FeatureTypeID="26">
                  <FeatureVersion ID="402676" ReliabilityID="1">
                    <Comment />
                    <VersionDetail DetailTypeID="1432">Operational and Military Leader of JIHAD GROUP</VersionDetail>
                  </FeatureVersion>
                  <IdentityReference IdentityID="4422" IdentityFeatureLinkTypeID="1" />
                </Feature>
              </Profile>
            </DistinctParty>
        </Root>
        "#;

        let mut des = Deserializer::from_str(xml);
        let result = crate::deserializers::deserialize_distinct_parties(&mut des);

        match result {
            Ok(ref list) => {
                assert_eq!(list.len(), 1);
                let (_id, actual) = list.iter().next().unwrap();
                assert_eq!(actual, &DistinctPartyTable { id: 2676 })
            }

            _ => {}
        }
    }
}
