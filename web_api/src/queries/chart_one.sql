WITH
    CITY_COUNTS AS (
        SELECT
            COUNT(*) AS CITY_COUNT,
            UPPER(CITY) AS CITY
        FROM
            PUBLIC.LOCATION
        WHERE
            CITY IS NOT NULL
        GROUP BY
            UPPER(CITY)
        HAVING
            COUNT(*) >= 1
    )
SELECT
    CITY,
    CITY_COUNT,
    1 + (
        (
            LOG(CITY_COUNT) / LOG(
                    (
                        SELECT
                            MAX(CITY_COUNT)
                        FROM
                            CITY_COUNTS
                    )
                              )
            ) * 99
        ) AS CITY_COUNT_NORMALIZED
FROM
    CITY_COUNTS
ORDER BY
    CITY_COUNT DESC;