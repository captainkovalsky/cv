// @generated automatically by Diesel CLI.

diesel::table! {
    alias_types (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    area_code_types (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    area_codes (id) {
        id -> Int8,
        country_id -> Int8,
        description -> Text,
        area_code_type_id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    calendar_types (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    countries (id) {
        id -> Int8,
        iso_2 -> Nullable<Text>,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    country_relevances (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    decision_making_bodies (id) {
        id -> Int8,
        organisation_id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    detail_references (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    feature_type_groups (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    feature_types (id) {
        id -> Int8,
        value -> Nullable<Text>,
        group_id -> Nullable<Text>,
    }
}

diesel::table! {
    legal_basises (id) {
        id -> Int8,
        type_id -> Int8,
        short_ref -> Nullable<Text>,
        sanctions_program_id -> Nullable<Text>,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    locations (id) {
        id -> Int8,
        location_area_code_id -> Int8,
        country_id -> Nullable<Int8>,
        country_relevance_id -> Nullable<Int8>,
        location_text -> Text,
        city -> Nullable<Text>,
        postal_code -> Nullable<Text>,
        address_1 -> Nullable<Text>,
        address_3 -> Nullable<Text>,
        state -> Nullable<Text>,
        region -> Nullable<Text>,
        address_2 -> Nullable<Text>,
    }
}

diesel::table! {
    reg_doc_documents (id) {
        id -> Int8,
        id_reg_doc_type_id -> Int8,
        validity_id -> Int8,
        identity_id -> Int8,
    }
}

diesel::table! {
    reg_doc_types (id) {
        id -> Int8,
        value -> Nullable<Text>,
    }
}

diesel::table! {
    distinct_parties(id) {
id -> Int8,
    }
}

diesel::joinable!(locations -> countries (country_id));
diesel::joinable!(locations -> country_relevances (country_relevance_id));

diesel::allow_tables_to_appear_in_same_query!(
    alias_types,
    area_code_types,
    area_codes,
    calendar_types,
    countries,
    country_relevances,
    decision_making_bodies,
    detail_references,
    feature_type_groups,
    feature_types,
    legal_basises,
    locations,
    reg_doc_documents,
    reg_doc_types,
);
