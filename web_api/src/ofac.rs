use crate::entities::DateOfIssue;
use std::collections::HashMap;
use std::io::{BufRead, ErrorKind};
use std::str::from_utf8;

use quick_xml::de::Deserializer;
use quick_xml::events::{BytesStart, Event};
use quick_xml::{Error, Reader, Writer};
use regex::Regex;
use reqwest::Response;
use select::document::Document;
use select::predicate::Name;
use serde::{Deserialize, Serialize};
use std::io;
use thiserror::Error;

mod constants {
    pub const FILE_NAME: &str = "sdn_advanced.xml";
    pub const HASH_URL: &str = "https://ofac.treasury.gov/specially-designated-nationals-list-sdn-list/hash-values-for-ofac-sanctions-list-files";
    #[cfg(not(debug_assertions))]
    pub const FILE_URL: &str =
        "https://www.treasury.gov/ofac/downloads/sanctions/1.0/sdn_advanced.xml";
    #[cfg(debug_assertions)]
    pub const FILE_LOCAL_URL: &str = "http://0.0.0.0:8000/sdn_advanced.xml";
}

#[derive(Error, Debug)]
pub enum OFACError {
    #[error("Failed to fetch data")]
    FetchFailed(#[from] reqwest::Error),
    #[error("failed to parse hash")]
    ParseHashError(#[from] io::Error),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Hashes {
    pub sha256: String,
    pub sha384: String,
    pub sha512: String,
}

#[derive(Serialize, Debug)]
pub struct OfacXmlTagInfo {
    pub xml_root_tag: String,
    pub xml_entity_tag: String,
    pub xml_raw_content: String,
}

#[derive(Serialize, Debug)]
pub struct OfacXmlData {
    pub tags_map: HashMap<String, OfacXmlTagInfo>,
    pub date_of_issue: DateOfIssue,
}

macro_rules! insert_into_map {
    ($reader:expr, $e:expr, $map:expr, $junk_buf:expr, $entity_tag:expr) => {
        let bytes = read_to_end_into_buffer($reader, &$e, $junk_buf)?;
        let xml_raw_content = from_utf8(&bytes)?;
        let root_tag = concat!($entity_tag, "Values");
        $map.insert(
            $entity_tag.parse().unwrap(),
            OfacXmlTagInfo {
                xml_root_tag: root_tag.to_string(),
                xml_entity_tag: $entity_tag.to_string(),
                xml_raw_content: xml_raw_content.clone().to_string(),
            },
        );
    };
}

// Reference: https://capnfabs.net/posts/parsing-huge-xml-quickxml-rust-serde/
// Reads from a start tag all the way to the corresponding end tag,
// Returns the bytes of the whole tag
fn read_to_end_into_buffer<R: BufRead>(
    reader: &mut Reader<R>,
    start_tag: &BytesStart,
    junk_buf: &mut Vec<u8>,
) -> Result<Vec<u8>, Error> {
    let mut depth = 0;
    let mut output_buf: Vec<u8> = Vec::new();
    let mut w = Writer::new(&mut output_buf);
    let tag_name = start_tag.name();
    w.write_event(Event::Start(start_tag.clone()))?;

    loop {
        junk_buf.clear();
        let event = reader.read_event_into(junk_buf)?;
        w.write_event(event.clone())?;

        match event {
            Event::Start(e) if e.name() == tag_name => depth += 1,
            Event::End(e) if e.name() == tag_name => {
                if depth == 0 {
                    return Ok(output_buf);
                }
                depth -= 1;
            }
            Event::Eof => {
                panic!("oh no")
            }
            _ => {}
        }
    }
}

pub async fn fetch_xml() -> reqwest::Result<Response> {
    #[cfg(not(debug_assertions))]
    let file = constants::FILE_URL;

    #[cfg(debug_assertions)]
    let file = constants::FILE_LOCAL_URL;

    println!("fetch document {}", file);
    reqwest::get(file).await
}

pub async fn fetch_hash_page() -> Result<Document, OFACError> {
    let res = reqwest::get(constants::HASH_URL)
        .await
        .map_err(OFACError::FetchFailed)?;

    let doc = res.text().await.map_err(OFACError::FetchFailed)?;
    let doc = Document::from(doc.as_str());
    Ok(doc)
}

pub fn parse_latest_hash(doc: Document) -> Result<Hashes, OFACError> {
    let articles = doc.find(Name("article"));
    let mut hashes: Option<Hashes> = None;

    for article in articles {
        let paragraphs = article.find(Name("p"));
        for paragraph in paragraphs {
            let text = paragraph.text();
            if text.contains(constants::FILE_NAME) {
                let re = Regex::new(
                    r"(?i)SHA-256: ([a-f\d]+)\s*SHA-384: ([a-f\d]+)\s*SHA-512: ([a-f\d]+)",
                )
                .unwrap();
                if let Some(captures) = re.captures(&text) {
                    let sha256 = captures.get(1).map_or("", |m| m.as_str());
                    let sha384 = captures.get(2).map_or("", |m| m.as_str());
                    let sha512 = captures.get(3).map_or("", |m| m.as_str());
                    hashes = Some(Hashes {
                        sha256: sha256.to_string(),
                        sha384: sha384.to_string(),
                        sha512: sha512.to_string(),
                    });
                    break;
                }
            }
        }
    }

    match hashes {
        Some(hashes) => Ok(hashes),
        None => Err(OFACError::ParseHashError(io::Error::new(
            ErrorKind::Other,
            "Failed to parse hash",
        ))),
    }
}

pub fn parse_xml(data: &String) -> Result<OfacXmlData, Error> {
    let mut reader = Reader::from_str(&data);
    let mut buf = Vec::new();
    let mut junk_buf: Vec<u8> = Vec::new();

    let mut date_of_issue: DateOfIssue = DateOfIssue {
        year: 0,
        month: 0,
        day: 0,
    };
    let mut xml_values: HashMap<String, OfacXmlTagInfo> = HashMap::new();

    loop {
        match reader.read_event_into(&mut buf) {
            Ok(Event::Eof) => break,
            Ok(Event::Start(e)) => match e.name().as_ref() {
                b"DateOfIssue" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "DateOfIssue"
                    );
                    let content = xml_values.get("DateOfIssue").unwrap();
                    let mut deserializer = Deserializer::from_str(&*content.xml_raw_content);
                    if let Ok(date) = DateOfIssue::deserialize(&mut deserializer) {
                        date_of_issue = date;
                    }
                }

                b"AreaCodeValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "AreaCode");
                }

                b"AliasTypeValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "AliasType");
                }

                b"AreaCodeTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "AreaCodeType"
                    );
                }

                b"CalendarTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "CalendarType"
                    );
                }

                b"CountryValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "Country");
                }
                b"CountryRelevanceValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "CountryRelevance"
                    );
                }
                b"DecisionMakingBodyValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "DecisionMakingBody"
                    );
                }
                b"DetailReferenceValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "DetailReference"
                    );
                }
                b"DetailTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "DetailType"
                    );
                }
                b"DocNameStatusValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "DocNameStatus"
                    );
                }
                b"EntryEventTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "EntryEventType"
                    );
                }
                b"FeatureTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "FeatureType"
                    );
                }
                b"FeatureTypeGroupValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "FeatureTypeGroup"
                    );
                }
                b"IDRegDocDateTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "IDRegDocDateType"
                    );
                }

                b"IDRegDocTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "IDRegDocType"
                    );
                }

                b"IdentityFeatureLinkTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "IdentityFeatureLinkType"
                    );
                }

                b"LegalBasisValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "LegalBasis"
                    );
                }

                b"LegalBasisTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "LegalBasisType"
                    );
                }

                b"ListValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "List");
                }

                b"LocPartTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "LocPartType"
                    );
                }

                b"LocPartValueStatusValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "LocPartValueStatus"
                    );
                }

                b"LocPartValueTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "LocPartValueType"
                    );
                }

                b"NamePartTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "NamePartType"
                    );
                }

                b"OrganisationValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "Organisation"
                    );
                }

                b"PartySubTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "PartySubType"
                    );
                }

                b"PartyTypeValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "PartyType");
                }

                b"RelationQualityValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "RelationQuality"
                    );
                }

                b"RelationTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "RelationType"
                    );
                }

                b"SanctionsProgramValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "SanctionsProgram"
                    );
                }

                b"SanctionsTypeValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "SanctionsType"
                    );
                }

                b"ScriptValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "Script");
                }

                b"ScriptStatusValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "ScriptStatus"
                    );
                }

                b"SubsidiaryBodyValues" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "SubsidiaryBody"
                    );
                }

                b"ValidityValues" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "Validity");
                }

                b"Locations" => {
                    insert_into_map!(&mut reader, &e, &mut xml_values, &mut junk_buf, "Location");
                }

                b"IDRegDocuments" => {
                    insert_into_map!(
                        &mut reader,
                        &e,
                        &mut xml_values,
                        &mut junk_buf,
                        "IDRegDocument"
                    );
                }
                _ => (),
            },
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            _ => (),
        }
    }

    return Ok(OfacXmlData {
        date_of_issue,
        tags_map: xml_values,
    });
}
