use diesel::{Associations, Identifiable, Insertable, Queryable, Selectable};
use serde::{Deserialize, Serialize};

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::alias_types)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct AliasTypeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::area_codes)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct AreaCodeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "@CountryID", serialize = "country_id"))]
    pub country_id: i64,
    #[serde(rename(deserialize = "@Description", serialize = "description"))]
    pub description: String,
    #[serde(rename(deserialize = "@AreaCodeTypeID", serialize = "area_code_type_id"))]
    pub area_code_type_id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::area_code_types)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct AreaCodeTypeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Identifiable, Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::country_relevances)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct CountryRelevanceTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::decision_making_bodies)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct DecisionMakingBodyTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "@OrganisationID", serialize = "organisation_id"))]
    pub organisation_id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::calendar_types)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct CalendarTypeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Identifiable, Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::countries)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct CountryTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "@ISO2", serialize = "iso_2"))]
    pub iso_2: Option<String>,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::detail_references)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct DetailReferenceTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::feature_types)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct FeatureTypeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename(deserialize = "@FeatureTypeGroupID", serialize = "group_id"))]
    pub group_id: Option<String>,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    pub value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::feature_type_groups)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct FeatureTypeGroupTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::reg_doc_types)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct RegDocTypeTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    id: i64,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    value: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone)]
#[diesel(table_name = crate::schema::legal_basises)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct LegalBasisTable {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    id: i64,
    #[serde(rename(deserialize = "@LegalBasisTypeID", serialize = "type_id"))]
    type_id: i64,
    #[serde(rename(deserialize = "@LegalBasisShortRef", serialize = "short_ref"))]
    short_ref: Option<String>,
    #[serde(rename(
        deserialize = "@SanctionsProgramID",
        serialize = "sanctions_program_id"
    ))]
    sanctions_program_id: Option<String>,
    #[serde(rename(deserialize = "$value", serialize = "value"))]
    value: Option<String>,
}

#[derive(
    Identifiable,
    Associations,
    Queryable,
    Selectable,
    Insertable,
    Deserialize,
    Serialize,
    Debug,
    Clone,
    PartialEq,
)]
#[diesel(table_name = crate::schema::locations)]
#[diesel(belongs_to(CountryTable, foreign_key = country_id ))]
#[diesel(belongs_to(CountryRelevanceTable, foreign_key = country_relevance_id ))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct LocationTable {
    pub id: i64,
    pub location_area_code_id: i64,
    pub country_id: Option<i64>,
    pub country_relevance_id: Option<i64>,
    pub location_text: String,
    pub region: Option<String>,
    pub address_1: Option<String>,
    pub address_2: Option<String>,
    pub address_3: Option<String>,
    pub city: Option<String>,
    pub state: Option<String>,
    pub postal_code: Option<String>,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone, PartialEq)]
#[diesel(table_name = crate::schema::reg_doc_documents)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct RegDocDocumentTable {
    pub id: i64,
    pub id_reg_doc_type_id: i64,
    pub identity_id: i64,
    pub validity_id: i64,
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug, Clone, PartialEq)]
#[diesel(table_name = crate::schema::distinct_parties)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct DistinctPartyTable {
    pub id: i64,
}
