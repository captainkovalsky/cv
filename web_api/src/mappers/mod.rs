use crate::entities::{LocationPart, LocationParts};
use std::collections::HashMap;
use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum XmlParseError {
    #[error("failed to parse locations")]
    ParseLocations(String),

    #[error("failed to parse reg documents")]
    ParseRegDocuments(String),
}

pub fn map_location_parts(parts: &[LocationPart]) -> HashMap<LocationParts, String> {
    let mut result = HashMap::new();

    for part in parts {
        for value in &part.location_part_value {
            let part_type = match part.loc_part_type_id {
                1450 => LocationParts::Region,
                1451 => LocationParts::Address1,
                1452 => LocationParts::Address2,
                1453 => LocationParts::Address3,
                1454 => LocationParts::City,
                1455 => LocationParts::State,
                1456 => LocationParts::PostalCode,
                _ => continue,
            };

            result.insert(part_type, value.value.clone());
        }
    }

    result
}
