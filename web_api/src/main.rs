mod deserializers;
mod entities;
mod mappers;
mod models;
mod ofac;
mod routes;
mod schema;
mod sql;

use axum::{routing::get, Router};
use serde::{Deserialize, Serialize};

use routes::api;
use routes::charts;
use routes::index;
use routes::latest_hash;
use routes::seed;

#[derive(Debug, Deserialize, Serialize)]
struct CvInfo {
    assets: String,
    cv: String,
}

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    let chart_routes = Router::new()
        .route("/chart-one", get(charts::chart_one))
        .route("/top-cities", get(charts::top_cities))
        .route("/top-countries", get(charts::top_countries));

    let manage_routes = Router::new()
        .route("/latest-hash", get(latest_hash::latest_hash))
        .route("/seed", get(seed::seed))
        .route("/test", get(seed::seed_test));

    let blog_routes = Router::new().route("/cv", get(api::get_cv_json));

    let dev_routes = Router::new().route("/cluster", get(charts::cluster_info));

    // build our application with a route
    let app = Router::new()
        .route("/", get(index::index))
        .route("/api", get(api::get_cv_json))
        .nest("/dev", dev_routes)
        .nest("/blog", blog_routes)
        .nest("/manage", manage_routes)
        .nest("/charts", chart_routes);

    // run our app with hyper, listening globally on port 3000
    let listener = tokio::net::TcpListener::bind("0.0.0.0:10000")
        .await
        .unwrap();
    axum::serve(listener, app).await.unwrap();
}
