use crate::ofac::{fetch_hash_page, parse_latest_hash};
use axum::Json;
use http::StatusCode;
use serde_json::{json, Value};

pub async fn latest_hash() -> (StatusCode, Json<Value>) {
    let result = fetch_hash_page();
    match result.await {
        Ok(doc) => match parse_latest_hash(doc) {
            Ok(res) => (StatusCode::OK, Json(json!(res))),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({
                    "message": err.to_string()
                })),
            ),
        },
        Err(err) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({
                    "message": err.to_string()
                })),
            )
        }
    }
}
