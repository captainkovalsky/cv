use crate::CvInfo;
use axum::Json;
use cv_parser::{parse_cv, Cv};
use std::env::current_dir;
use std::fs;
pub async fn get_cv_json() -> Json<Cv> {
    let mut current_dir = current_dir().expect("ok");

    let static_info: CvInfo = CvInfo {
        assets: "assets/".to_string(),
        cv: "cv.yaml".to_string(),
    };

    current_dir.push(static_info.assets.clone());
    current_dir.push(static_info.cv.clone());
    let file_path = current_dir.to_str().expect("failed to get file path");
    let content = fs::read_to_string(file_path).expect("failed to read CV file");
    let cv: Cv = parse_cv(content.as_str()).expect("ok");
    Json(cv)
}
