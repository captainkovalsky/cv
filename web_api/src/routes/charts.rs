use crate::sql;
use axum::extract::Query;
use axum::Json;
use cluster::clasterise;
use colored::Colorize;
use http::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;
use thiserror::Error;
#[derive(Error, Serialize, Debug)]
pub enum ChartError {
    #[error("failed to create {0} Chart")]
    Chart(String),
}

pub async fn chart_one() -> (StatusCode, Json<Value>) {
    let connection = &mut sql::connection::establish_connection();
    let results = sql::get_location_rows(connection);
    match results {
        Ok(list) => {
            println!("{} {:?}", "ok".green(), list.len());

            return (
                StatusCode::OK,
                Json(json!({
                    "ok": "ok",
                    "results": list
                })),
            );
        }
        Err(_err) => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({
                  "error": ChartError::Chart("first".to_string()).to_string(),
                })),
            );
        }
    }
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
struct Params {
    #[serde(default)]
    k: Option<i32>,
}

pub async fn cluster_info(Query(params): Query<HashMap<String, usize>>) -> Json<Value> {
    #[derive(Serialize)]
    pub struct ClusterJsonResponse {
        ok: String,
        cluster: HashMap<String, Vec<String>>,
    }

    let k = params.get("k").cloned().unwrap_or(2);

    let connection = &mut sql::connection::establish_connection();
    let results = sql::get_location_rows(connection).unwrap();
    let mut cities: Vec<String> = results.iter().map(|row| row.city.clone()).collect();
    cities.sort();

    let cluster: HashMap<String, Vec<String>> = clasterise(cities.clone(), k);

    Json(json!(ClusterJsonResponse {
        ok: "ok".to_string(),
        cluster
    }))
}

pub async fn top_cities() -> Json<Value> {
    let _connection = &mut sql::connection::establish_connection();

    Json(json!({
        "ok": true,
        "locations": [],
    }))
}

pub async fn top_countries() -> Json<Value> {
    Json(json!({
        "ok": true,
    }))
}

pub async fn cities_by_country(Query(params): Query<HashMap<String, String>>) -> Json<Value> {
    let city = params.get("city").cloned().unwrap_or(String::from(""));
    println!("City {}", city);
    todo!("implement");
}

pub async fn top_industries() -> Json<Value> {
    todo!("implement");
}

pub async fn top_industries_by_country() -> Json<Value> {
    todo!("implement");
}

pub async fn map_data() -> Json<Value> {
    todo!("implement map data");
}
