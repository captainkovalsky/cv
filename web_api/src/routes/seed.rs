use crate::deserializers::{deserialize_locations, deserialize_reg_documents};
use crate::models::{LocationTable, RegDocDocumentTable};
use crate::ofac::{fetch_xml, parse_xml};
use crate::sql;
use crate::sql::seed::{seed_reference_table_by_key, SeedChunkTable};
use crate::sql::truncate_all;
use axum::Json;
use colored::Colorize;
use http::StatusCode;
use quick_xml::de::Deserializer;
use serde_json::{json, Value};
use std::collections::HashMap;

pub async fn seed() -> (StatusCode, Json<Value>) {
    let connection = &mut sql::connection::establish_connection();
    let data = fetch_xml().await.expect("ok").text().await.unwrap();
    let xml_data = parse_xml(&data).unwrap();
    let ok = truncate_all(connection);
    match ok {
        Ok(_) => {
            println!("{}", "truncate DB".green());
        }
        Err(err) => {
            eprintln!("{} {:?}", "failed to truncate db".red(), err);
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({
                    "error": "failed DB operation"
                })),
            );
        }
    }

    for (key, _value) in xml_data.tags_map.iter() {
        let mut deserializer = Deserializer::from_str(&*_value.xml_raw_content);
        if let Some(closure) = seed_reference_table_by_key(key, &mut deserializer) {
            match closure(connection) {
                Ok(_) => {
                    println!("seed {}", key);
                }
                Err(err) => {
                    println!("{} {} {:?}", "failed to seed {}".red(), key, err);
                }
            }
        };
    }

    for (key, _value) in xml_data.tags_map.iter() {
        match key.as_str() {
            "Location" => {
                let mut des = Deserializer::from_str(&*_value.xml_raw_content);
                let result = deserialize_locations(&mut des);
                match result {
                    Ok(list) => {
                        let count = LocationTable::seed(
                            list.values().cloned().collect::<Vec<LocationTable>>(),
                            connection,
                        );
                        match count {
                            Ok(size) => {
                                println!("{} {}", "location inserted ".green(), size);
                            }
                            Err(err) => {
                                eprintln!("{} {:?}", "[ERROR]".red(), err);
                            }
                        }
                    }
                    Err(err) => {
                        eprintln!("{} {:?}", "[ERROR]".red(), err);
                    }
                }
            }
            "IDRegDocument" => {
                let mut des = Deserializer::from_str(&*_value.xml_raw_content);
                let result = deserialize_reg_documents(&mut des);
                match result {
                    Ok(list) => {
                        let count = RegDocDocumentTable::seed(
                            list.values().cloned().collect::<Vec<RegDocDocumentTable>>(),
                            connection,
                        );
                        match count {
                            Ok(size) => {
                                println!("{} {}", "documents inserted ".green(), size);
                            }
                            Err(err) => {
                                eprintln!("{} {:?}", "[ERROR]".red(), err);
                            }
                        }
                    }
                    Err(err) => {
                        eprintln!("{} {:?}", "[ERROR]".red(), err);
                    }
                }
            }
            _s => {
                // println!("unmatched {:?}", s);
            }
        }
    }

    (
        StatusCode::OK,
        Json(json!({
            "ok": "ok",
        })),
    )
}

pub async fn seed_test() -> Json<Value> {
    let _connection = &mut sql::connection::establish_connection();
    let _data = fetch_xml().await.expect("ok").text().await.unwrap();
    let _outer_list: HashMap<i64, RegDocDocumentTable> = HashMap::new();
    Json(json!({
        "ok": "ok"
    }))
}
