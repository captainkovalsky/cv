pub mod link;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct DateOfIssue {
    #[serde(rename = "Year")]
    pub(crate) year: u32,
    #[serde(rename = "Month")]
    pub(crate) month: u32,
    #[serde(rename = "Day")]
    pub(crate) day: u32,
}

#[derive(Debug, PartialEq, Hash, Eq)]
pub enum LocationParts {
    Region = 1450,
    Address1 = 1451,
    Address2 = 1452,
    Address3 = 1453,
    City = 1454,
    State = 1455,
    PostalCode = 1456,
}
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct LocationCountry {
    #[serde(rename(deserialize = "@CountryID", serialize = "country_id"))]
    pub country_id: i64,
    #[serde(rename(
        deserialize = "@CountryRelevanceID",
        serialize = "country_relevance_id"
    ))]
    pub country_relevance_id: i64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct LocationAreaCode {
    #[serde(rename(deserialize = "@AreaCodeID", serialize = "area_code_id"))]
    pub area_code_id: i64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Location {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,
    #[serde(rename = "LocationAreaCode")]
    pub location_area_code: Option<LocationAreaCode>,
    #[serde(rename = "LocationPart")]
    pub location_parts: Option<Vec<LocationPart>>,
    #[serde(rename = "LocationCountry")]
    pub location_country: Option<LocationCountry>,
    #[serde(rename = "FeatureVersionReference")]
    pub feature_version_references: Option<Vec<FeatureVersionReference>>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct LocationPart {
    #[serde(rename(deserialize = "@LocPartTypeID", serialize = "loc_part_type_id"))]
    pub loc_part_type_id: i64,
    #[serde(rename = "LocationPartValue")]
    pub location_part_value: Vec<LocationPartValue>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct LocationPartValue {
    #[serde(rename(deserialize = "@Primary", serialize = "primary"))]
    pub primary: bool,
    #[serde(rename(
        deserialize = "@LocPartValueTypeID",
        serialize = "loc_part_value_type_id"
    ))]
    pub loc_part_value_type_id: i64,
    #[serde(rename(
        deserialize = "@LocPartValueStatusID",
        serialize = "loc_part_value_status_id"
    ))]
    pub loc_part_value_status_id: i64,
    #[serde(rename = "Value")]
    pub value: String,
    pub comment: Option<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct FeatureVersionReference {
    #[serde(rename(deserialize = "@FeatureVersionID", serialize = "feature_version_id"))]
    pub feature_version_id: i64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Locations {
    #[serde(rename = "Location")]
    pub locations: Vec<Location>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RegDocument {
    #[serde(rename(deserialize = "@ID", serialize = "id"))]
    pub id: i64,

    #[serde(rename(deserialize = "@IDRegDocTypeID", serialize = "id_reg_doc_type_id"))]
    pub id_reg_doc_type_id: i64,

    #[serde(rename(deserialize = "@IdentityID", serialize = "identity_id"))]
    pub identity_id: i64,

    #[serde(rename(deserialize = "@ValidityID", serialize = "validity_id"))]
    pub validity_id: i64,
}
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RegDocuments {
    #[serde(rename = "IDRegDocument")]
    pub documents: Vec<RegDocument>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct DistinctParty {
    #[serde(rename(deserialize = "@FixedRef", serialize = "id"))]
    pub id: i64,
}
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct DistinctParties {
    #[serde(rename = "DistinctParty")]
    pub parties: Vec<DistinctParty>,
}

mod profile_relationships {
    use serde::Deserialize;

    #[derive(Deserialize, Debug, Clone)]
    pub struct ProfileRelationship {
        pub id: u32,
        pub from_profile_id: u32,
        pub to_profile_id: u32,
        pub relation_type_id: u32,
        pub relation_quality_id: u32,
        pub former: bool,
        pub sanctions_entry_id: u32,
        pub comment: Option<String>,
        pub date_period: Option<DatePeriod>,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct DatePeriod {
        pub calendar_type_id: u32,
        pub year_fixed: bool,
        pub month_fixed: bool,
        pub day_fixed: bool,
        pub comment: Option<String>,
        pub start: Option<StartEnd>,
        pub end: Option<StartEnd>,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct StartEnd {
        pub approximate: bool,
        pub year_fixed: bool,
        pub month_fixed: bool,
        pub day_fixed: bool,
        pub from: Option<Date>,
        pub to: Option<Date>,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Date {
        pub year: u32,
        pub month: u32,
        pub day: u32,
    }
}

pub mod reference_value_sets {
    use crate::models::{
        AliasTypeTable, AreaCodeTable, AreaCodeTypeTable, CalendarTypeTable, CountryRelevanceTable,
        CountryTable, DecisionMakingBodyTable, DetailReferenceTable, FeatureTypeGroupTable,
        FeatureTypeTable, LegalBasisTable, RegDocTypeTable,
    };
    use serde::{Deserialize, Serialize};

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AliasTypeValues {
        #[serde(rename = "AliasType")]
        pub list: Vec<AliasTypeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AreaCodeValues {
        #[serde(rename = "AreaCode")]
        pub list: Vec<AreaCodeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AreaCodeTypeValues {
        #[serde(rename = "AreaCodeType")]
        pub list: Vec<AreaCodeTypeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct DecisionMakingBodyValues {
        #[serde(rename = "DecisionMakingBody")]
        pub list: Vec<DecisionMakingBodyTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CalendarTypeValues {
        #[serde(rename = "CalendarType")]
        pub list: Vec<CalendarTypeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CountryRelevanceValues {
        #[serde(rename = "CountryRelevance")]
        pub list: Vec<CountryRelevanceTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct CountryValues {
        #[serde(rename = "Country")]
        pub list: Vec<CountryTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct DetailReferenceValues {
        #[serde(rename = "DetailReference")]
        pub list: Vec<DetailReferenceTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct RegDocTypeValues {
        #[serde(rename = "IDRegDocType")]
        pub list: Vec<RegDocTypeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct FeatureTypeGroupValues {
        #[serde(rename = "FeatureTypeGroup")]
        pub list: Vec<FeatureTypeGroupTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct FeatureTypeValues {
        #[serde(rename = "FeatureType")]
        pub list: Vec<FeatureTypeTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct LegalBasisValues {
        #[serde(rename = "LegalBasis")]
        pub list: Vec<LegalBasisTable>,
    }

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct DetailType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct DocNameStatus {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct EntryEventType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct IDRegDocDateType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct IDRegDocType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct IdentityFeatureLinkType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct LegalBasisType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct List {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct LocPartType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct LocPartValueStatus {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct LocPartValueType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct NamePartType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Organisation {
        pub id: u32,
        pub country_id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct PartySubType {
        pub id: u32,
        pub party_type_id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct PartyType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct RelationQuality {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct RelationType {
        pub id: u32,
        pub symmetrical: bool,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Reliability {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct SanctionsProgram {
        pub id: u32,
        pub subsidiary_body_id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct SanctionsType {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Script {
        pub id: u32,
        pub script_code: String,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct ScriptStatus {
        pub id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct SubsidiaryBody {
        pub id: u32,
        pub notional: bool,
        pub decision_making_body_id: u32,
        pub value: String,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Validity {
        pub id: u32,
        pub value: String,
    }
}

// fdfd
mod sanctions_entries {
    use serde::Deserialize;

    #[derive(Deserialize, Debug, Clone)]
    pub struct SanctionsEntry {
        pub id: u32,
        pub profile_id: u32,
        pub list_id: u32,
        pub entry_event: EntryEvent,
        pub sanctions_measures: Vec<SanctionsMeasure>,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct EntryEvent {
        pub id: u32,
        pub entry_event_type_id: u32,
        pub legal_basis_id: u32,
        pub comment: Option<String>,
        pub date: Date,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct Date {
        pub calendar_type_id: u32,
        pub year: u32,
        pub month: u32,
        pub day: u32,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct SanctionsMeasure {
        pub id: u32,
        pub sanctions_type_id: u32,
        pub comment: Option<String>,
        pub date_period: DatePeriod,
    }

    #[derive(Deserialize, Debug, Clone)]
    pub struct DatePeriod {
        pub calendar_type_id: u32,
        pub year_fixed: bool,
        pub month_fixed: bool,
        pub day_fixed: bool,
    }
}
