use serde::Serialize;

#[derive(Serialize)]
pub struct Link {
    pub href: String,
    pub title: String,
}

impl Link {
    pub fn new<A1, A2>(href: A1, title: A2) -> Self
    where
        A1: Into<String>,
        A2: Into<String>,
    {
        Link {
            href: href.into(),
            title: title.into(),
        }
    }
}
