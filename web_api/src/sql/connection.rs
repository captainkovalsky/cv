use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::{dotenv, from_filename};
use std::env;

pub fn establish_connection() -> PgConnection {
    let environment = env::var("RENDER").unwrap_or_else(|_| "development".to_string());
    if environment == "development" {
        from_filename(".env.development.local").ok();
    } else {
        dotenv().ok();
    }

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}
