use crate::entities::reference_value_sets::{
    AliasTypeValues, AreaCodeTypeValues, AreaCodeValues, CalendarTypeValues,
    CountryRelevanceValues, CountryValues, DecisionMakingBodyValues, DetailReferenceValues,
    FeatureTypeGroupValues, FeatureTypeValues, LegalBasisValues, RegDocTypeValues,
};
use crate::models::{DistinctPartyTable, LocationTable, RegDocDocumentTable};
use crate::schema::{
    alias_types, area_code_types, area_codes, calendar_types, countries, country_relevances,
    decision_making_bodies, detail_references, distinct_parties, feature_type_groups,
    feature_types, legal_basises, locations, reg_doc_documents, reg_doc_types,
};
use diesel::result::{Error as DieselError, Error};
use diesel::{PgConnection, RunQueryDsl};
use serde::Deserialize;

pub trait SeedChunkTable<T> {
    fn seed(list: Vec<T>, connection: &mut PgConnection) -> Result<usize, DieselError>;
}

pub trait SeedReferenceTable<'a, T> {
    fn seed(&'a self, connection: &mut PgConnection) -> Result<usize, DieselError>;
}

macro_rules! impl_seed_table {
    ($type:ident, $table:ident) => {
        impl<'a> SeedReferenceTable<'a, $type> for $type {
            fn seed(
                &'a self,
                connection: &mut PgConnection,
            ) -> Result<usize, diesel::result::Error> {
                diesel::insert_into($table::table)
                    .values(self.list.to_owned())
                    .execute(connection)
            }
        }
    };
}

macro_rules! impl_seed_chunks_table {
    ($type:ident, $table:ident) => {
        impl SeedChunkTable<$type> for $type {
            fn seed(
                list: Vec<$type>,
                connection: &mut PgConnection,
            ) -> Result<usize, diesel::result::Error> {
                let chunk_size = 500;
                let mut total_size = 0;

                for chunk in list.chunks(chunk_size) {
                    let size = diesel::insert_into($table::table)
                        .values(chunk)
                        .execute(connection);

                    match size {
                        Ok(count) => {
                            total_size += count;
                            println!(
                                "seed progress {:.2} %",
                                (total_size * 100) as f32 / list.len() as f32
                            );
                        }
                        Err(err) => {
                            eprintln!("constraint {:?}", err);
                        }
                    }
                }

                Ok(total_size)
            }
        }
    };
}

impl_seed_chunks_table!(LocationTable, locations);
impl_seed_chunks_table!(RegDocDocumentTable, reg_doc_documents);
impl_seed_chunks_table!(DistinctPartyTable, distinct_parties);

impl_seed_table!(AreaCodeValues, area_codes);
impl_seed_table!(AliasTypeValues, alias_types);
impl_seed_table!(AreaCodeTypeValues, area_code_types);
impl_seed_table!(DecisionMakingBodyValues, decision_making_bodies);
impl_seed_table!(CalendarTypeValues, calendar_types);
impl_seed_table!(CountryRelevanceValues, country_relevances);
impl_seed_table!(CountryValues, countries);
impl_seed_table!(FeatureTypeValues, feature_types);
impl_seed_table!(DetailReferenceValues, detail_references);
impl_seed_table!(FeatureTypeGroupValues, feature_type_groups);
impl_seed_table!(RegDocTypeValues, reg_doc_types);
impl_seed_table!(LegalBasisValues, legal_basises);

pub fn seed_reference_table_by_key<'a, 'b, R>(
    key: &str,
    deserializer: &'a mut quick_xml::de::Deserializer<'b, R>,
) -> Option<Box<dyn FnOnce(&mut PgConnection) -> Result<usize, Error> + 'a>>
where
    R: quick_xml::de::XmlRead<'b>,
{
    let closure: Box<dyn FnOnce(&mut PgConnection) -> Result<usize, Error> + 'a> = match key {
        "AreaCode" => Box::new(|connection| {
            AreaCodeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "AliasType" => Box::new(|connection| {
            AliasTypeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "AreaCodeType" => Box::new(|connection| {
            AreaCodeTypeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "DecisionMakingBody" => Box::new(|connection| {
            DecisionMakingBodyValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "CalendarType" => Box::new(|connection| {
            CalendarTypeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "CountryRelevance" => Box::new(|connection| {
            CountryRelevanceValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "Country" => Box::new(|connection| {
            CountryValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "FeatureType" => Box::new(|connection| {
            FeatureTypeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "DetailReference" => Box::new(|connection| {
            DetailReferenceValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "FeatureTypeGroup" => Box::new(|connection| {
            FeatureTypeGroupValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "IDRegDocType" => Box::new(|connection| {
            RegDocTypeValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        "LegalBasis" => Box::new(|connection| {
            LegalBasisValues::deserialize(&mut *deserializer)
                .expect("ok")
                .seed(connection)
        }),
        _ => return None,
    };
    Some(closure)
}
