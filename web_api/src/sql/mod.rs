use crate::schema::alias_types::dsl::alias_types;
use crate::schema::area_code_types::dsl::area_code_types;
use crate::schema::area_codes::dsl::area_codes;
use crate::schema::calendar_types::dsl::calendar_types;
use crate::schema::countries::dsl::countries;
use crate::schema::country_relevances::dsl::country_relevances;
use crate::schema::decision_making_bodies::dsl::decision_making_bodies;
use crate::schema::detail_references::dsl::detail_references;
use crate::schema::feature_type_groups::dsl::feature_type_groups;
use crate::schema::feature_types::dsl::feature_types;
use crate::schema::legal_basises::dsl::legal_basises;
use crate::schema::locations::dsl::locations;
use crate::schema::reg_doc_documents::dsl::reg_doc_documents;
use crate::schema::reg_doc_types::dsl::reg_doc_types;
use diesel::sql_types::*;
use diesel::{sql_query, PgConnection, QueryableByName, RunQueryDsl};
use serde::Serialize;

pub(crate) mod connection;
pub mod seed;

pub fn truncate_all(conn: &mut PgConnection) -> Result<(), diesel::result::Error> {
    diesel::delete(alias_types).execute(conn)?;
    diesel::delete(area_codes).execute(conn)?;
    diesel::delete(area_code_types).execute(conn)?;
    diesel::delete(decision_making_bodies).execute(conn)?;
    diesel::delete(calendar_types).execute(conn)?;
    diesel::delete(detail_references).execute(conn)?;
    diesel::delete(feature_types).execute(conn)?;
    diesel::delete(feature_type_groups).execute(conn)?;
    diesel::delete(reg_doc_types).execute(conn)?;
    diesel::delete(legal_basises).execute(conn)?;
    diesel::delete(locations).execute(conn)?;
    diesel::delete(countries).execute(conn)?;
    diesel::delete(country_relevances).execute(conn)?;
    diesel::delete(reg_doc_documents).execute(conn)?;
    Ok(())
}

#[derive(QueryableByName, Serialize, Debug)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct RowInfo {
    #[diesel(sql_type = Text)]
    pub city: String,
    #[diesel(sql_type = Int8)]
    pub city_count: i64,
    #[diesel(sql_type = Float8)]
    pub city_count_normalized: f64,
}

pub fn get_location_rows(
    connection: &mut PgConnection,
) -> Result<Vec<RowInfo>, diesel::result::Error> {
    let query = r#"
WITH
	CITY_COUNTS AS (
		SELECT
			COUNT(*) AS CITY_COUNT,
			UPPER(CITY) AS CITY
		FROM
			PUBLIC.LOCATIONS
		WHERE
			CITY IS NOT NULL
		GROUP BY
			UPPER(CITY)
		HAVING
			COUNT(*) >= 1
	)
SELECT
	CITY,
	CITY_COUNT,
	1 + (
		(
			LOG(CITY_COUNT) / LOG(
				(
					SELECT
						MAX(CITY_COUNT)
					FROM
						CITY_COUNTS
				)
			)
		) * 99
	) AS CITY_COUNT_NORMALIZED
FROM
	CITY_COUNTS
ORDER BY
	CITY_COUNT DESC;
    "#;

    let results = sql_query(query).get_results::<RowInfo>(connection);

    results
}
