use std::collections::HashMap;

use serde::{Deserialize, Deserializer, Serialize};
use serde_yaml::Value;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Country {
    #[serde(rename = "UA")]
    Ua,
    #[serde(rename = "USA")]
    Usa,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct CompanyInfo {
    name: String,
    city: String,
    country: Option<Country>,
    site: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Project {
    name: String,
    site: String,
    description: String,
    hq: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Contacts {
    skype: String,
    email: String,
    linkedin: String,
    github: String,
    phone: Vec<String>,
}

#[derive(Deserialize, Serialize, PartialEq, Debug)]
struct Experience {
    position: String,
    project: Project,
    stack: Vec<String>,
    responsibilities: Vec<String>,
    start: String,
    end: Option<String>,
    #[serde(deserialize_with = "deserialize_company")]
    company: String,
}
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Cv {
    name: String,
    surname: String,
    contacts: Contacts,
    experience: Vec<Experience>,
    #[serde(default)]
    companies: HashMap<String, CompanyInfo>,
}

fn deserialize_company<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    let value: Value = Deserialize::deserialize(deserializer)?;

    if let Some(reference) = value.as_str() {
        Ok(reference.to_string())
    } else if let Some(map) = value.as_mapping() {
        let name = map
            .get(&Value::String("name".to_string()))
            .and_then(Value::as_str)
            .unwrap_or_default();

        Ok(name.to_string())
    } else {
        Err(serde::de::Error::custom(
            "Invalid company, please check YAML",
        ))
    }
}

pub fn parse_cv(content: &str) -> Result<Cv, serde_yaml::Error> {
    let cv: Cv = serde_yaml::from_str(content).expect("failed to parse CV");
    Ok(cv)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_cv_test() {
        let mock_yaml = r#"
name: Viktor
surname: Dzundza
contacts:
  skype: victor.dzundza
  phone:
    - "+1"
    - "+2"
  email: 
  linkedin: 
  github: 
experience:
  - position: A
    company: A
    project:
      name: A
      site: A
      description: A
      hq: A
    stack:
      - React
    responsibilities:
      - A
    start: "2021-12-01"
    end: "2023-12-31"
"#;
        let cv = parse_cv(mock_yaml).unwrap();
        assert_eq!(cv.name, "Viktor");
        assert_eq!(cv.surname, "Dzundza");
        assert_eq!(cv.contacts.skype, "victor.dzundza");
        assert_eq!(cv.contacts.phone.len(), 2);
        assert_eq!(cv.experience.len(), 1);
    }
}
