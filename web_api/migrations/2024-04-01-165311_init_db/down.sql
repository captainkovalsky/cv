-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS "feature_type_group";
DROP TABLE IF EXISTS "decision_making_body";
DROP TABLE IF EXISTS "calendar_type";
DROP TABLE IF EXISTS "area_code";
DROP TABLE IF EXISTS "detail_reference";
DROP TABLE IF EXISTS "legal_basis";
DROP TABLE IF EXISTS "reg_doc_type";
DROP TABLE IF EXISTS "alias_type";
DROP TABLE IF EXISTS "area_code_type";
DROP TABLE IF EXISTS "feature_type";
DROP TABLE IF EXISTS "location";
DROP TABLE IF EXISTS "country";
DROP TABLE IF EXISTS "country_relevance";
