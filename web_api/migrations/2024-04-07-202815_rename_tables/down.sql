-- This file should undo anything in `up.sql`
CREATE TABLE "alias_type"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "area_code"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"country_id" INT8 NOT NULL,
	"description" TEXT NOT NULL,
	"area_code_type_id" INT8 NOT NULL,
	"value" TEXT
);

CREATE TABLE "area_code_type"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "calendar_type"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "country"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"iso_2" TEXT,
	"value" TEXT
);

CREATE TABLE "country_relevance"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "decision_making_body"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"organisation_id" INT8 NOT NULL,
	"value" TEXT
);

CREATE TABLE "detail_reference"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "feature_type"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT,
	"group_id" TEXT
);

CREATE TABLE "feature_type_group"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "legal_basis"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"type_id" INT8 NOT NULL,
	"short_ref" TEXT,
	"sanctions_program_id" TEXT,
	"value" TEXT
);

CREATE TABLE "location"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"location_area_code_id" INT8 NOT NULL,
	"country_id" INT8,
	"country_relevance_id" INT8,
	"location_text" TEXT NOT NULL,
	"city" TEXT,
	"postal_code" TEXT,
	"address_1" TEXT,
	"address_3" TEXT,
	"state" TEXT,
	"region" TEXT,
	"address_2" TEXT,
	FOREIGN KEY ("country_id") REFERENCES "country"("id"),
	FOREIGN KEY ("country_relevance_id") REFERENCES "country_relevance"("id")
);

CREATE TABLE "reg_doc_document"(
	"id" INT8 NOT NULL PRIMARY KEY
);

CREATE TABLE "reg_doc_type"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

DROP TABLE IF EXISTS "detail_references";
DROP TABLE IF EXISTS "feature_type_groups";
DROP TABLE IF EXISTS "decision_making_bodies";
DROP TABLE IF EXISTS "alias_types";
DROP TABLE IF EXISTS "feature_types";
DROP TABLE IF EXISTS "legal_basises";
DROP TABLE IF EXISTS "reg_doc_documents";
DROP TABLE IF EXISTS "reg_doc_types";
DROP TABLE IF EXISTS "area_code_types";
DROP TABLE IF EXISTS "area_codes";
DROP TABLE IF EXISTS "calendar_types";
DROP TABLE IF EXISTS "locations";
DROP TABLE IF EXISTS "countries";
DROP TABLE IF EXISTS "country_relevances";
