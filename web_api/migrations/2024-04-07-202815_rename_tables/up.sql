-- Your SQL goes here
DROP TABLE IF EXISTS "alias_type";
DROP TABLE IF EXISTS "area_code";
DROP TABLE IF EXISTS "area_code_type";
DROP TABLE IF EXISTS "calendar_type";
DROP TABLE IF EXISTS "decision_making_body";
DROP TABLE IF EXISTS "detail_reference";
DROP TABLE IF EXISTS "feature_type";
DROP TABLE IF EXISTS "feature_type_group";
DROP TABLE IF EXISTS "legal_basis";
DROP TABLE IF EXISTS "reg_doc_document";
DROP TABLE IF EXISTS "reg_doc_type";
DROP TABLE IF EXISTS "location";
DROP TABLE IF EXISTS "country";
DROP TABLE IF EXISTS "country_relevance";

CREATE TABLE "detail_references"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "feature_type_groups"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "decision_making_bodies"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"organisation_id" INT8 NOT NULL,
	"value" TEXT
);

CREATE TABLE "country_relevances"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "alias_types"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "feature_types"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT,
	"group_id" TEXT
);

CREATE TABLE "countries"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"iso_2" TEXT,
	"value" TEXT
);

CREATE TABLE "legal_basises"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"type_id" INT8 NOT NULL,
	"short_ref" TEXT,
	"sanctions_program_id" TEXT,
	"value" TEXT
);

CREATE TABLE "locations"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"location_area_code_id" INT8 NOT NULL,
	"country_id" INT8,
	"country_relevance_id" INT8,
	"location_text" TEXT NOT NULL,
	"city" TEXT,
	"postal_code" TEXT,
	"address_1" TEXT,
	"address_3" TEXT,
	"state" TEXT,
	"region" TEXT,
	"address_2" TEXT,
	FOREIGN KEY ("country_id") REFERENCES "countries"("id"),
	FOREIGN KEY ("country_relevance_id") REFERENCES "country_relevances"("id")
);

CREATE TABLE "reg_doc_documents"(
	"id" INT8 NOT NULL PRIMARY KEY
);

CREATE TABLE "reg_doc_types"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "area_code_types"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

CREATE TABLE "area_codes"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"country_id" INT8 NOT NULL,
	"description" TEXT NOT NULL,
	"area_code_type_id" INT8 NOT NULL,
	"value" TEXT
);

CREATE TABLE "calendar_types"(
	"id" INT8 NOT NULL PRIMARY KEY,
	"value" TEXT
);

