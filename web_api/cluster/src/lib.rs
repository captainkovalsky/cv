use clustering::*;
use colored::Colorize;
use std::collections::HashMap;
use std::time::Instant;
use text_distance;
use text_distance::JaroWinkler;

pub fn clasterise(city_list: Vec<String>, k: usize) -> HashMap<String, Vec<String>> {
    let city_list_lower: Vec<String> = city_list.iter().map(|city| city.to_lowercase()).collect();

    let mut matrix_low: Vec<Vec<f64>> = Vec::with_capacity(city_list_lower.len());
    let start_time = Instant::now();

    for (i, city) in city_list_lower.iter().enumerate() {
        let mut row = Vec::with_capacity(city_list_lower.len());

        for city_next in &city_list_lower[i..] {
            let value = if city == city_next {
                0.0
            } else {
                let ja = JaroWinkler {
                    src: city.to_owned(),
                    tar: city_next.to_owned(),
                    winklerize: true,
                };
                ja.distance()
            };

            row.push(value);
        }

        for j in 0..i {
            let symmetric_index = i - j - 1;

            if symmetric_index < row.len() {
                row.insert(0, row[symmetric_index]);
            } else {
                row.insert(0, 0.0);
            }
        }
        matrix_low.push(row);
    }
    let end_time = Instant::now();

    let elapsed_time = end_time - start_time;
    println!(
        "{} Elapsed time: {:?}",
        "[JaroWinkler]".green(),
        elapsed_time
    );

    let max_iter = 1;

    let clustering = kmeans(k, &matrix_low, max_iter);

    let mut cluster_map: HashMap<String, Vec<String>> = HashMap::with_capacity(k - 1);

    for (i, &cluster_index) in clustering.membership.iter().enumerate() {
        let city = &city_list_lower[i];
        let cluster_key = format!("cluster_{}", cluster_index);
        let cities_in_cluster = cluster_map.entry(cluster_key).or_insert(Vec::new());
        cities_in_cluster.push(city.to_owned());
    }

    println!("k={}", k);

    cluster_map.clone()
}
