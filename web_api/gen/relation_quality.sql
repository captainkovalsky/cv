DROP TABLE IF EXISTS relation_quality_temp;

CREATE TABLE relation_quality_temp AS SELECT
    xml $$
        <RelationQualityValues><RelationQuality ID="1540">High</RelationQuality><RelationQuality ID="1541">Low</RelationQuality><RelationQuality ID="1542">on OFAC's SDN list:</RelationQuality><RelationQuality ID="1">Unknown</RelationQuality></RelationQualityValues>
$$ AS data;

DROP TABLE IF EXISTS relation_quality;

CREATE TABLE relation_quality AS
SELECT xmltable.*
FROM relation_quality_temp,
    XMLTABLE('//{{root}}/RelationQuality'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS relation_quality_temp;
