DROP TABLE IF EXISTS entry_event_type_temp;

CREATE TABLE entry_event_type_temp AS SELECT
    xml $$
        <EntryEventTypeValues><EntryEventType ID="1">Created</EntryEventType></EntryEventTypeValues>
$$ AS data;

DROP TABLE IF EXISTS entry_event_type;

CREATE TABLE entry_event_type AS
SELECT xmltable.*
FROM entry_event_type_temp,
    XMLTABLE('//{{root}}/EntryEventType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS entry_event_type_temp;
