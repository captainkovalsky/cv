DROP TABLE IF EXISTS loc_part_value_status_temp;

CREATE TABLE loc_part_value_status_temp AS SELECT
    xml $$
        <LocPartValueStatusValues><LocPartValueStatus ID="1">Main</LocPartValueStatus></LocPartValueStatusValues>
$$ AS data;

DROP TABLE IF EXISTS loc_part_value_status;

CREATE TABLE loc_part_value_status AS
SELECT xmltable.*
FROM loc_part_value_status_temp,
    XMLTABLE('//{{root}}/LocPartValueStatus'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS loc_part_value_status_temp;
