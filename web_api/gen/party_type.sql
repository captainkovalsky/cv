DROP TABLE IF EXISTS party_type_temp;

CREATE TABLE party_type_temp AS SELECT
    xml $$
        <PartyTypeValues><PartyType ID="1">Individual</PartyType><PartyType ID="2">Entity</PartyType><PartyType ID="3">Location</PartyType><PartyType ID="4">Transport</PartyType><PartyType ID="5">Other Entity</PartyType></PartyTypeValues>
$$ AS data;

DROP TABLE IF EXISTS party_type;

CREATE TABLE party_type AS
SELECT xmltable.*
FROM party_type_temp,
    XMLTABLE('//{{root}}/PartyType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS party_type_temp;
