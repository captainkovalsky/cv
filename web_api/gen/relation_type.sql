DROP TABLE IF EXISTS relation_type_temp;

CREATE TABLE relation_type_temp AS SELECT
    xml $$
        <RelationTypeValues><RelationType ID="1555" Symmetrical="false">Associate Of</RelationType><RelationType ID="15001" Symmetrical="false">Providing support to</RelationType><RelationType ID="15002" Symmetrical="false">Acting for or on behalf of</RelationType><RelationType ID="15003" Symmetrical="false">Owned or Controlled By</RelationType><RelationType ID="15004" Symmetrical="false">Family member of</RelationType><RelationType ID="91422" Symmetrical="false">playing a significant role in</RelationType><RelationType ID="91725" Symmetrical="false">Leader or official of</RelationType><RelationType ID="91900" Symmetrical="false">Principal Executive Officer</RelationType><RelationType ID="92019" Symmetrical="false">Owns, controls, or operates</RelationType><RelationType ID="92122" Symmetrical="false">Property in the interest of</RelationType></RelationTypeValues>
$$ AS data;

DROP TABLE IF EXISTS relation_type;

CREATE TABLE relation_type AS
SELECT xmltable.*
FROM relation_type_temp,
    XMLTABLE('//{{root}}/RelationType'
             PASSING data COLUMNS
        symmetrical bool PATH 'Symmetrical'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS relation_type_temp;
