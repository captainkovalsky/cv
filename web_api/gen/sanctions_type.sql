DROP TABLE IF EXISTS sanctions_type_temp;

CREATE TABLE sanctions_type_temp AS SELECT
    xml $$
        <SanctionsTypeValues><SanctionsType ID="1705">Block</SanctionsType><SanctionsType ID="1706">Reject</SanctionsType><SanctionsType ID="91419">NDAA Special Conditions</SanctionsType><SanctionsType ID="91505">13662 Sectoral Directive 1</SanctionsType><SanctionsType ID="91506">13662 Sectoral Directive 2</SanctionsType><SanctionsType ID="91513">13662 Sectoral Directive 3</SanctionsType><SanctionsType ID="91514">13662 Sectoral Directive 4</SanctionsType><SanctionsType ID="92087">Executive Order 14024 Directive 3</SanctionsType><SanctionsType ID="92093">Executive Order 14024 Directive 2</SanctionsType><SanctionsType ID="1">Program</SanctionsType></SanctionsTypeValues>
$$ AS data;

DROP TABLE IF EXISTS sanctions_type;

CREATE TABLE sanctions_type AS
SELECT xmltable.*
FROM sanctions_type_temp,
    XMLTABLE('//{{root}}/SanctionsType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS sanctions_type_temp;
