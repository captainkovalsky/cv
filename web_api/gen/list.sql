DROP TABLE IF EXISTS list_temp;

CREATE TABLE list_temp AS SELECT
    xml $$
        <ListValues><List ID="1550">SDN List</List><List ID="91243">Non-SDN Palestinian Legislative Council List</List><List ID="91507">Sectoral Sanctions Identifications List</List><List ID="91512">Consolidated List</List></ListValues>
$$ AS data;

DROP TABLE IF EXISTS list;

CREATE TABLE list AS
SELECT xmltable.*
FROM list_temp,
    XMLTABLE('//{{root}}/List'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS list_temp;
