DROP TABLE IF EXISTS subsidiary_body_temp;

CREATE TABLE subsidiary_body_temp AS SELECT
    xml $$
        <SubsidiaryBodyValues><SubsidiaryBody ID="1" Notional="false" DecisionMakingBodyID="1">Office of Foreign Assets Control</SubsidiaryBody></SubsidiaryBodyValues>
$$ AS data;

DROP TABLE IF EXISTS subsidiary_body;

CREATE TABLE subsidiary_body AS
SELECT xmltable.*
FROM subsidiary_body_temp,
    XMLTABLE('//{{root}}/SubsidiaryBody'
             PASSING data COLUMNS
        notional bool PATH 'Notional',
decision_making_body_id text PATH 'DecisionMakingBodyID'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS subsidiary_body_temp;
