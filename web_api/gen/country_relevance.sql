DROP TABLE IF EXISTS country_relevance_temp;

CREATE TABLE country_relevance_temp AS SELECT
    xml $$
        <CountryRelevanceValues><CountryRelevance ID="1413">PRIMARY</CountryRelevance></CountryRelevanceValues>
$$ AS data;

DROP TABLE IF EXISTS country_relevance;

CREATE TABLE country_relevance AS
SELECT xmltable.*
FROM country_relevance_temp,
    XMLTABLE('//{{root}}/CountryRelevance'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS country_relevance_temp;
