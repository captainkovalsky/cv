DROP TABLE IF EXISTS validity_temp;

CREATE TABLE validity_temp AS SELECT
    xml $$
        <ValidityValues><Validity ID="1">Valid</Validity><Validity ID="2">Fraudulent</Validity></ValidityValues>
$$ AS data;

DROP TABLE IF EXISTS validity;

CREATE TABLE validity AS
SELECT xmltable.*
FROM validity_temp,
    XMLTABLE('//{{root}}/Validity'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS validity_temp;
