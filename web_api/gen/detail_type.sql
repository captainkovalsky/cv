DROP TABLE IF EXISTS detail_type_temp;

CREATE TABLE detail_type_temp AS SELECT
    xml $$
        <DetailTypeValues><DetailType ID="1430">DATE</DetailType><DetailType ID="1431">LOOKUP</DetailType><DetailType ID="1432">TEXT</DetailType><DetailType ID="1433">COUNTRY</DetailType><DetailType ID="1434">LOCATION</DetailType></DetailTypeValues>
$$ AS data;

DROP TABLE IF EXISTS detail_type;

CREATE TABLE detail_type AS
SELECT xmltable.*
FROM detail_type_temp,
    XMLTABLE('//{{root}}/DetailType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS detail_type_temp;
