DROP TABLE IF EXISTS loc_part_type_temp;

CREATE TABLE loc_part_type_temp AS SELECT
    xml $$
        <LocPartTypeValues><LocPartType ID="1">Unknown</LocPartType><LocPartType ID="1450">REGION</LocPartType><LocPartType ID="1451">ADDRESS1</LocPartType><LocPartType ID="1452">ADDRESS2</LocPartType><LocPartType ID="1453">ADDRESS3</LocPartType><LocPartType ID="1454">CITY</LocPartType><LocPartType ID="1455">STATE/PROVINCE</LocPartType><LocPartType ID="1456">POSTAL CODE</LocPartType></LocPartTypeValues>
$$ AS data;

DROP TABLE IF EXISTS loc_part_type;

CREATE TABLE loc_part_type AS
SELECT xmltable.*
FROM loc_part_type_temp,
    XMLTABLE('//{{root}}/LocPartType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS loc_part_type_temp;
