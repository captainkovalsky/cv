DROP TABLE IF EXISTS calendar_type_temp;

CREATE TABLE calendar_type_temp AS SELECT
    xml $$
        <CalendarTypeValues><CalendarType ID="1">Gregorian</CalendarType></CalendarTypeValues>
$$ AS data;

DROP TABLE IF EXISTS calendar_type;

CREATE TABLE calendar_type AS
SELECT xmltable.*
FROM calendar_type_temp,
    XMLTABLE('//{{root}}/CalendarType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS calendar_type_temp;
