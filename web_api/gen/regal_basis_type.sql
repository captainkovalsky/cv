DROP TABLE IF EXISTS regal_basis_type_temp;

CREATE TABLE regal_basis_type_temp AS SELECT
    xml $$
        <LegalBasisTypeValues><LegalBasisType ID="1">Unknown</LegalBasisType></LegalBasisTypeValues>
$$ AS data;

DROP TABLE IF EXISTS regal_basis_type;

CREATE TABLE regal_basis_type AS
SELECT xmltable.*
FROM regal_basis_type_temp,
    XMLTABLE('//{{root}}/LegalBasisType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS regal_basis_type_temp;
