DROP TABLE IF EXISTS identity_feature_link_type_temp;

CREATE TABLE identity_feature_link_type_temp AS SELECT
    xml $$
        <IdentityFeatureLinkTypeValues><IdentityFeatureLinkType ID="1">Unknown</IdentityFeatureLinkType></IdentityFeatureLinkTypeValues>
$$ AS data;

DROP TABLE IF EXISTS identity_feature_link_type;

CREATE TABLE identity_feature_link_type AS
SELECT xmltable.*
FROM identity_feature_link_type_temp,
    XMLTABLE('//{{root}}/IdentityFeatureLinkType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS identity_feature_link_type_temp;
