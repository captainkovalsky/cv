DROP TABLE IF EXISTS area_code_type_temp;

CREATE TABLE area_code_type_temp AS SELECT
    xml $$
        <AreaCodeTypeValues><AreaCodeType ID="1">ISO 3166 (2)</AreaCodeType></AreaCodeTypeValues>
$$ AS data;

DROP TABLE IF EXISTS area_code_type;

CREATE TABLE area_code_type AS
SELECT xmltable.*
FROM area_code_type_temp,
    XMLTABLE('//{{root}}/AreaCodeType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS area_code_type_temp;
