DROP TABLE IF EXISTS script_temp;

CREATE TABLE script_temp AS SELECT
    xml $$
        <ScriptValues><Script ID="160" ScriptCode="Arab">Arabic</Script><Script ID="230" ScriptCode="Armn">Armenian</Script><Script ID="501" ScriptCode="Hans">Chinese Simplified</Script><Script ID="502" ScriptCode="Hant">Chinese Traditional</Script><Script ID="220" ScriptCode="Cyrl">Cyrillic</Script><Script ID="240" ScriptCode="Geor">Georgian</Script><Script ID="200" ScriptCode="Grek">Greek</Script><Script ID="125" ScriptCode="Hebrew">Hebrew</Script><Script ID="413" ScriptCode="Japanese">Japanese</Script><Script ID="215" ScriptCode="Latin">Latin</Script><Script ID="287" ScriptCode="Korean">Korean</Script></ScriptValues>
$$ AS data;

DROP TABLE IF EXISTS script;

CREATE TABLE script AS
SELECT xmltable.*
FROM script_temp,
    XMLTABLE('//{{root}}/Script'
             PASSING data COLUMNS
        script_code text PATH 'ScriptCode'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS script_temp;
