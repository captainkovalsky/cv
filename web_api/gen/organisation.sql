DROP TABLE IF EXISTS organisation_temp;

CREATE TABLE organisation_temp AS SELECT
    xml $$
        <OrganisationValues><Organisation ID="1" CountryID="11212">State</Organisation></OrganisationValues>
$$ AS data;

DROP TABLE IF EXISTS organisation;

CREATE TABLE organisation AS
SELECT xmltable.*
FROM organisation_temp,
    XMLTABLE('//{{root}}/Organisation'
             PASSING data COLUMNS
        country_id text PATH 'CountryID'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS organisation_temp;
