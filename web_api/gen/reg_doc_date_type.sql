DROP TABLE IF EXISTS reg_doc_date_type_temp;

CREATE TABLE reg_doc_date_type_temp AS SELECT
    xml $$
        <IDRegDocDateTypeValues><IDRegDocDateType ID="1480">Issue Date</IDRegDocDateType><IDRegDocDateType ID="1481">Expiration Date</IDRegDocDateType></IDRegDocDateTypeValues>
$$ AS data;

DROP TABLE IF EXISTS reg_doc_date_type;

CREATE TABLE reg_doc_date_type AS
SELECT xmltable.*
FROM reg_doc_date_type_temp,
    XMLTABLE('//{{root}}/IDRegDocDateType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS reg_doc_date_type_temp;
