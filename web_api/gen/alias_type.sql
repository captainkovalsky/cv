DROP TABLE IF EXISTS alias_type_temp;

CREATE TABLE alias_type_temp AS SELECT
    xml $$
        <AliasTypeValues><AliasType ID="1400">A.K.A.</AliasType><AliasType ID="1401">F.K.A.</AliasType><AliasType ID="1402">N.K.A.</AliasType><AliasType ID="1403">Name</AliasType></AliasTypeValues>
$$ AS data;

DROP TABLE IF EXISTS alias_type;

CREATE TABLE alias_type AS
SELECT xmltable.*
FROM alias_type_temp,
    XMLTABLE('//{{root}}/AliasType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS alias_type_temp;
