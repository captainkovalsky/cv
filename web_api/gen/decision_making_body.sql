DROP TABLE IF EXISTS decision_making_body_temp;

CREATE TABLE decision_making_body_temp AS SELECT
    xml $$
        <DecisionMakingBodyValues><DecisionMakingBody ID="1" OrganisationID="1">United States Treasury Department</DecisionMakingBody></DecisionMakingBodyValues>
$$ AS data;

DROP TABLE IF EXISTS decision_making_body;

CREATE TABLE decision_making_body AS
SELECT xmltable.*
FROM decision_making_body_temp,
    XMLTABLE('//{{root}}/DecisionMakingBody'
             PASSING data COLUMNS
        organisation_id text PATH 'OrganisationID'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS decision_making_body_temp;
