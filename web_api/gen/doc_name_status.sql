DROP TABLE IF EXISTS doc_name_status_temp;

CREATE TABLE doc_name_status_temp AS SELECT
    xml $$
        <DocNameStatusValues><DocNameStatus ID="1">Primary Latin</DocNameStatus><DocNameStatus ID="2">Others</DocNameStatus></DocNameStatusValues>
$$ AS data;

DROP TABLE IF EXISTS doc_name_status;

CREATE TABLE doc_name_status AS
SELECT xmltable.*
FROM doc_name_status_temp,
    XMLTABLE('//{{root}}/DocNameStatus'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS doc_name_status_temp;
