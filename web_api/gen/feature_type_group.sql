DROP TABLE IF EXISTS feature_type_group_temp;

CREATE TABLE feature_type_group_temp AS SELECT
    xml $$
        <FeatureTypeGroupValues><FeatureTypeGroup ID="1">Unknown</FeatureTypeGroup></FeatureTypeGroupValues>
$$ AS data;

DROP TABLE IF EXISTS feature_type_group;

CREATE TABLE feature_type_group AS
SELECT xmltable.*
FROM feature_type_group_temp,
    XMLTABLE('//{{root}}/FeatureTypeGroup'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS feature_type_group_temp;
