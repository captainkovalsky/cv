DROP TABLE IF EXISTS script_status_temp;

CREATE TABLE script_status_temp AS SELECT
    xml $$
        <ScriptStatusValues><ScriptStatus ID="1">Unknown</ScriptStatus></ScriptStatusValues>
$$ AS data;

DROP TABLE IF EXISTS script_status;

CREATE TABLE script_status AS
SELECT xmltable.*
FROM script_status_temp,
    XMLTABLE('//{{root}}/ScriptStatus'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS script_status_temp;
