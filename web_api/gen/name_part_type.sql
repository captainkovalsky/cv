DROP TABLE IF EXISTS name_part_type_temp;

CREATE TABLE name_part_type_temp AS SELECT
    xml $$
        <NamePartTypeValues><NamePartType ID="1520">Last Name</NamePartType><NamePartType ID="1521">First Name</NamePartType><NamePartType ID="1522">Middle Name</NamePartType><NamePartType ID="1523">Maiden Name</NamePartType><NamePartType ID="1524">Aircraft Name</NamePartType><NamePartType ID="1525">Entity Name</NamePartType><NamePartType ID="1526">Vessel Name</NamePartType><NamePartType ID="1528">Nickname</NamePartType><NamePartType ID="91708">Patronymic</NamePartType><NamePartType ID="91709">Matronymic</NamePartType></NamePartTypeValues>
$$ AS data;

DROP TABLE IF EXISTS name_part_type;

CREATE TABLE name_part_type AS
SELECT xmltable.*
FROM name_part_type_temp,
    XMLTABLE('//{{root}}/NamePartType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS name_part_type_temp;
