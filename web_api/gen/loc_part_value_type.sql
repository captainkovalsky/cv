DROP TABLE IF EXISTS loc_part_value_type_temp;

CREATE TABLE loc_part_value_type_temp AS SELECT
    xml $$
        <LocPartValueTypeValues><LocPartValueType ID="1">Unknown</LocPartValueType></LocPartValueTypeValues>
$$ AS data;

DROP TABLE IF EXISTS loc_part_value_type;

CREATE TABLE loc_part_value_type AS
SELECT xmltable.*
FROM loc_part_value_type_temp,
    XMLTABLE('//{{root}}/LocPartValueType'
             PASSING data COLUMNS
        
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS loc_part_value_type_temp;
