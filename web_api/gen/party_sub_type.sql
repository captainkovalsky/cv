DROP TABLE IF EXISTS party_sub_type_temp;

CREATE TABLE party_sub_type_temp AS SELECT
    xml $$
        <PartySubTypeValues><PartySubType ID="1" PartyTypeID="4">Vessel</PartySubType><PartySubType ID="2" PartyTypeID="4">Aircraft</PartySubType><PartySubType ID="3" PartyTypeID="2">Unknown</PartySubType><PartySubType ID="4" PartyTypeID="1">Unknown</PartySubType></PartySubTypeValues>
$$ AS data;

DROP TABLE IF EXISTS party_sub_type;

CREATE TABLE party_sub_type AS
SELECT xmltable.*
FROM party_sub_type_temp,
    XMLTABLE('//{{root}}/PartySubType'
             PASSING data COLUMNS
        party_type_id text PATH 'PartyTypeID'
             id int PATH '@ID',
                 ordinality FOR ORDINALITY,
                 value text PATH 'text()'
);

-- Clean temp table
DROP TABLE IF EXISTS party_sub_type_temp;
