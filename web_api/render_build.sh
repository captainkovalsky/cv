#!/bin/bash

cargo build --release
cp -vr ./../../data/assets /opt/render/project/src/apps/web_api/assets
cd /opt/render/project/src/apps/web_api/assets
ls -la
cd -