export async function fetchCv(): Promise<any> {
  return fetch(`${process.env.FQDN}/api`, { next: { revalidate: 1 } })
    .then((response) => response.json())
    .catch((err) => {
      console.error(err, 'failed to fetch');
      return null;
    });
}
