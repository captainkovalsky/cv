export interface ICompany {
  name: string;
  city: string;
  country?: ICountry;
  site: string;
}

export enum ICountry {
  Ua = 'UA',
}

export interface IContacts {
  skype: string;
  phone: string;
  email: string;
  linkedin: string;
  github: string;
}

export interface IExperience {
  position: string;
  company: ICompany;
  project: IProject;
  stack: string[];
  responsibilities: string[];
  start: Date;
  end: Date;
  hide?: boolean;
}

export interface IProject {
  name: string;
  site: string;
  description: string;
  hq: string;
}
