import React from 'react';
import type { Metadata } from 'next';
import { Montserrat } from 'next/font/google';
import App from './app';
import './globals.scss';

const font = Montserrat({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Viktor Site',
  description: 'Personal WebSite',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <body style={{ ...font.style }}>
        <App>{children}</App>
      </body>
    </html>
  );
}
