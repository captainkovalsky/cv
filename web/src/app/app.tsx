'use client';

import React, { useState } from 'react';
import {
  FlexGrid,
  HeaderContainer,
  Header,
  SkipToContent,
  HeaderName,
  HeaderMenuButton,
  HeaderNavigation,
  HeaderMenuItem,
  HeaderMenu,
  Row,
  HeaderSideNavItems,
  SideNav,
  SideNavItems,
  Content,
  HeaderGlobalAction,
  HeaderGlobalBar,
  Theme,
} from '@carbon/react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { PdfReference, PaintBrush } from '@carbon/icons-react';

const title = `[Viktor Site]`;

const Links = () => {
  const path = usePathname();

  return (
    <>
      <HeaderMenuItem href='/cv' as={Link} isActive={path === '/cv'}>
        CV
      </HeaderMenuItem>
      <HeaderMenuItem href='/about' as={Link} isActive={path === '/about'}>
        About
      </HeaderMenuItem>
      <HeaderMenuItem href='/game' as={Link} isActive={path === '/game'}>
        Game
      </HeaderMenuItem>
      <HeaderMenu aria-label='Download CV' menuLinkName='Download'>
        <HeaderMenuItem
          href='/Viktor Dzundza - CV.pdf'
          target='_blank'
          rel='noopener noreferrer'
        >
          CV &nbsp; <PdfReference />
        </HeaderMenuItem>
      </HeaderMenu>
    </>
  );
};

const themes = ['g90', 'g100', 'white'];
export default function App({ children }: { children: React.ReactNode }) {
  const [currentTheme, setCurrentTheme] = useState<number>(1);
  return (
    //@ts-ignore
    <Theme theme={themes[currentTheme - 1]} style={{ minHeight: '100vh' }}>
      <HeaderContainer
        render={({
          isSideNavExpanded,
          onClickSideNavExpand,
        }: {
          isSideNavExpanded: boolean;
          onClickSideNavExpand: () => void;
        }) => (
          <Header aria-label='Viktor Personal Site'>
            <SkipToContent />
            <HeaderMenuButton
              aria-label={isSideNavExpanded ? 'Close menu' : 'Open menu'}
              onClick={onClickSideNavExpand}
              isActive={isSideNavExpanded}
              aria-expanded={isSideNavExpanded}
            />
            <HeaderName href='#' prefix=''>
              {title}
            </HeaderName>
            <HeaderNavigation aria-label={title}>
              <Links />
            </HeaderNavigation>
            <SideNav
              aria-label='Side navigation'
              expanded={isSideNavExpanded}
              isPersistent={false}
              onSideNavBlur={onClickSideNavExpand}
            >
              <SideNavItems>
                <HeaderSideNavItems>
                  <Links />
                </HeaderSideNavItems>
              </SideNavItems>
            </SideNav>
            <HeaderGlobalBar>
              <HeaderGlobalAction
                aria-label='Theme Switcher'
                tooltipAlignment='end'
              >
                <PaintBrush
                  size={20}
                  onClick={() =>
                    setCurrentTheme((theme) => (theme % themes.length) + 1)
                  }
                />
              </HeaderGlobalAction>
            </HeaderGlobalBar>
          </Header>
        )}
      />
      <Content>
        <FlexGrid>
          <Row>{children}</Row>
        </FlexGrid>
      </Content>
    </Theme>
  );
}
