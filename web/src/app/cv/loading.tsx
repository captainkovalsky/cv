'use client';
import { Column, Loading } from '@carbon/react';

export default function loading() {
  return (
    <Column
      xlg={{ offset: 6, span: 2 }}
      style={{
        margin: '0 auto',
      }}
    >
      <Loading active={true} withOverlay={false} />
    </Column>
  );
}
