'use client';
import {
  Column,
  ExpandableTile,
  FlexGrid,
  Link as CarbonLink,
  Row,
  Tag,
  TileAboveTheFoldContent,
  TileBelowTheFoldContent,
} from '@carbon/react';
import React from 'react';
import DateComponent from '@/app/cv/components/date';
import TwoColumns from '@/app/cv/components/two-columns/two-columns';

interface CVProps {
  cv: any;
}

interface CompanyLinkProps {
  name: string;
  link: string;
}

const CompanyLink = ({ name, link }: CompanyLinkProps) => {
  return (
    <CarbonLink style={{ fontStyle: 'italic' }} href={link} target={'_blank'}>
      {name}
    </CarbonLink>
  );
};

interface ProjectViewProps {
  exp: any;
  companies: any;
}
const ProjectView = ({ exp, companies = {} }: ProjectViewProps) => {
  return [
    <u key={`${exp.start}-${exp.end}`}>
      <DateComponent start={exp.start} end={exp.end} delimiter='-' />
    </u>,
    <React.Fragment key={exp?.project?.name}>
      <span style={{ fontWeight: 'bold' }}>{exp?.position}</span>
      ,&ensp;
      {exp?.company === 'Contract' ? (
        <CompanyLink name={exp?.project?.name} link={exp?.project?.site} />
      ) : (
        <CompanyLink
          name={exp?.company}
          link={companies?.[exp?.company]?.site}
        />
      )}
      , &ensp;
      <span>{companies?.[exp?.company]?.city}</span>
    </React.Fragment>,
  ];
};

interface TagsProps {
  exp: any;
}

const Tags = ({ exp }: TagsProps) => {
  return (
    <div style={{ marginLeft: '-6px' }}>
      {exp?.stack.map((title: string) => (
        <Tag type='blue' title={title} key={title}>
          {title}
        </Tag>
      ))}
    </div>
  );
};
export default function CvView({ cv }: CVProps) {
  return (
    <FlexGrid id='cv'>
      <Row>
        <Column>
          <h2>
            {cv?.name} {cv?.surname}
          </h2>
        </Column>
      </Row>
      <Row>
        <Column>
          <p>Work Experience</p>
        </Column>
      </Row>
      <Row as={'section'}>
        {cv?.experience.map((exp: any, index: number) => {
          return (
            <ExpandableTile
              tileMaxHeight={80}
              key={index}
              id='expandable-tile-1'
              tileCollapsedIconText='Interact to Expand tile'
              tileExpandedIconText='Interact to Collapse tile'
            >
              <TileAboveTheFoldContent>
                <TwoColumns>
                  {...ProjectView({ companies: cv?.companies, exp: exp })}
                </TwoColumns>
              </TileAboveTheFoldContent>
              <TileBelowTheFoldContent>
                <TwoColumns>
                  <Tags exp={exp} />
                </TwoColumns>
              </TileBelowTheFoldContent>
            </ExpandableTile>
          );
        })}
      </Row>
    </FlexGrid>
  );
}
