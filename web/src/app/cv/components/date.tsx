import React from 'react';
const options = {
  year: 'numeric',
  month: 'short',
} as Intl.DateTimeFormatOptions;

function formatDate(dateStr: string) {
  return new Date(dateStr).toLocaleDateString(undefined, options);
}

interface Props {
  start: string;
  end: string;
  delimiter?: string;
}

function DateComponent({ start, end, delimiter = '-' }: Props) {
  const formattedStart = formatDate(start);
  const formattedEnd = end ? formatDate(end) : 'Present';
  return <>{`${formattedStart} ${delimiter} ${formattedEnd}`}</>;
}

export default DateComponent;
