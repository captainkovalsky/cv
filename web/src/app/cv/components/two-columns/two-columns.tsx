import React from 'react';
import styles from './two-columns.module.scss';

interface TwoColumnsProps {
  children: React.ReactNode | React.ReactNode[];
}

export default function TwoColumns({ children }: TwoColumnsProps) {
  if (React.Children.count(children) === 1) {
    return (
      <div className={styles.root}>
        <div className={styles.first} />
        <div className={styles.second}>{children}</div>
      </div>
    );
  }

  const [firstChild, secondChild] = React.Children.toArray(children);

  return (
    <div className={styles.root}>
      <div className={styles.first}>{firstChild}</div>
      <div className={styles.second}>{secondChild}</div>
    </div>
  );
}
