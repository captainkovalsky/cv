'use client';
import React from 'react';
import { Column } from '@carbon/react';

export default function cv({ children }: { children: React.ReactNode }) {
  return <Column lg={{ span: 10, offset: 3 }}>{children}</Column>;
}
