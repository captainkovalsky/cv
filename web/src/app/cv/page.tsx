import { Suspense } from 'react';
import { fetchCv } from '@/service/cv';
import CvView from '@/app/cv/components/cv';
export const dynamic = 'auto';

export default async function CV() {
  const cv = await fetchCv();
  return (
    <Suspense>
      <CvView cv={cv} />
    </Suspense>
  );
}
