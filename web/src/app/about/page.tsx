export default function AboutPage() {
  return (
    <div id='about'>
      <p>
        As an accomplished frontend engineer, I have gained diverse experience
        across various sectors, including Travel Tech, Cloud Data Analytics,
        Digital IP Infrastructure, and Drone Management. While adept in my
        specialization, {`I'm`} not confined to a single framework and am
        enthusiastic about leveraging modern technology. I am versatile, open to
        roles beyond the front end, and thrive when given opportunities to
        diversify my technical repertoire. Passionate and organized, I excel in
        challenging environments and am eager to take on positions that not only
        harness my existing expertise but also pave the way for new learnings
        and explorations in the tech realm.
      </p>
    </div>
  );
}
