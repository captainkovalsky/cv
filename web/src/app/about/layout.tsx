'use client';
import React from 'react';
import { Column } from '@carbon/react';

export default function about({ children }: { children: React.ReactNode }) {
  return <Column lg={{ span: 10, offset: 3 }}>{children}</Column>;
}
