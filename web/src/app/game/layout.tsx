'use client';
import React from 'react';

export default function game({ children }: { children: React.ReactNode }) {
  return <>{children}</>;
}
