'use client';
import React from 'react';
import init from '@/lib/was_lib';
import { Loading } from '@carbon/react';

async function load() {
  if (typeof window !== 'undefined') {
    return await init().catch((error) => {
      if (!error.message.startsWith('Using exceptions for control flow,')) {
        throw error;
      }
    });
  }
}
export default function GamePage() {
  const [loading, setLoading] = React.useState(true);
  React.useEffect(() => {
    (async () => {
      await load().finally(() => {
        setLoading(false);
      });
    })();
  }, []);
  return (
    <>
      {loading && <Loading active={true} withOverlay={true} small={true} />}
      <canvas
        id='bevy'
        style={{ border: '0.5px solid blue', width: '100%', height: '100%' }}
      ></canvas>
    </>
  );
}
