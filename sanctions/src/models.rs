use serde::Deserialize;

#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct CityFrequencyChart {
    city: String,
    city_count: i64,
    city_count_normalized: f64,
}

#[derive(Clone, PartialEq, Deserialize, Debug)]
pub struct ChartResponse<T> {
    ok: String,
    results: Vec<T>,
}
