use crate::app::Root;
use log::Level;

mod app;
mod contexts;
mod models;

mod constants {
    #[cfg(not(debug_assertions))]
    pub const FQDN: &str = "https://web-service-iyvo.onrender.com";
    #[cfg(debug_assertions)]
    pub const FQDN: &str = "http://127.0.0.1:7777";
}

fn main() {
    console_log::init_with_level(Level::Trace).expect("Failed to initialise Log!");
    yew::Renderer::<Root>::new().render();
}
