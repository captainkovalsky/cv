use crate::{
    constants::FQDN,
    contexts::{use_theme, ThemeKind, ThemeProvider},
    models::{ChartResponse, CityFrequencyChart},
};
use charming::{
    component::Legend,
    element::ItemStyle,
    series::{Pie, PieRoseType},
    Chart, WasmRenderer,
};
use gloo_net::http::Request;
use log::{error, info};
use serde::Deserialize;
use stylist::css;
use stylist::yew::Global;
use yew::prelude::*;
use yew_hooks::prelude::*;

#[derive(Clone, Debug, PartialEq)]
struct Theme {
    theme: String,
}

#[derive(Clone, PartialEq, Deserialize, Debug)]
struct Data {
    year: i32,
    price: f32,
    #[serde(rename = "CPU model")]
    cpu_model: String,
    #[serde(rename = "Hard disk size")]
    hard_disk_size: String,
}
#[derive(Clone, PartialEq, Deserialize, Debug)]
struct ResponseDto {
    id: String,
    name: String,
    data: Data,
}

#[function_component(Switcher)]
pub fn switcher() -> Html {
    let theme = use_theme();

    let theme_str = match theme.kind() {
        ThemeKind::Light => "Dark Theme",
        ThemeKind::Dark => "Light Theme",
    };

    let other_theme = match theme.kind() {
        ThemeKind::Light => ThemeKind::Dark,
        ThemeKind::Dark => ThemeKind::Light,
    };

    let switch_theme = Callback::from(move |_| theme.set(other_theme.clone()));

    html! {
        <div>
            <button class={css!(r#"color: white;
                height: 50px;
                width: 300px;
                font-size: 20px;
                background-color: rgb(88, 164, 255);
                border-radius: 5px;
                border: none;

            "#)} onclick={switch_theme} id="yew-sample-button">{"Switch to "}{theme_str}</button>
        </div>
    }
}

#[function_component(App)]
pub fn app() -> Html {
    let theme = use_theme();
    use_effect_with((), move |_| {
        wasm_bindgen_futures::spawn_local(async move {
            let request_result = Request::get(format!("{}/charts/chart-one", FQDN).as_str())
                .send()
                .await;

            match request_result {
                Ok(response) => {
                    let json_result = response.json::<ChartResponse<CityFrequencyChart>>().await;
                    match json_result {
                        Ok(json) => {
                            info!("fetched data {:?}", json);
                        }
                        Err(err) => {
                            error!("failed to deserialize JSON: {:?}", err);
                        }
                    }
                }
                Err(err) => {
                    error!("failed to send request: {:?}", err);
                }
            };
        })
    });

    let init_chart = use_async::<_, _, ()>({
        let chart = Chart::new().legend(Legend::new().top("bottom")).series(
            Pie::new()
                .name("Sanctions Frequency by City: Location Breakdown")
                .rose_type(PieRoseType::Area)
                .radius(vec!["50", "250"])
                .center(vec!["50%", "50%"])
                .item_style(ItemStyle::new().border_radius(8))
                .data(vec![
                    (40.0, "rose 1"),
                    (38.0, "rose 2"),
                    (32.0, "rose 3"),
                    (30.0, "rose 4"),
                    (28.0, "rose 5"),
                    (26.0, "rose 6"),
                    (22.0, "rose 7"),
                    (18.0, "rose 8"),
                ]),
        );

        async move {
            let renderer = WasmRenderer::new(650, 600);
            renderer.render("chart", &chart).unwrap();
            Ok(())
        }
    });

    use_effect_with((), move |_| {
        init_chart.run();
        || ()
    });

    html! {
        <>

       <Global css={css!(
                r#"
                    html, body {
                        min-height: 100vh;
                        background-color: ${bg};
                        color: ${ft_color};
                    }
                "#,
                bg = theme.background_color.clone(),
                ft_color = theme.font_color.clone(),
            )} />
        <main>
            <h1>{ "Russia Sanctions List" }</h1>
            <div id="chart"></div>
            <span class="subtitle">{ "from Yew with " }<i class="heart" /></span>
                    <Switcher />
        </main>
        </>
    }
}

#[function_component(Root)]
pub fn root() -> Html {
    html! {
        <ThemeProvider>
            <App />
        </ThemeProvider>
    }
}
